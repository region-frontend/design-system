# Contributing
## How to start?
We use StorybookUI for development of our components.

#### Install
Clone the project from the repository and install all dependencies using `yarn`
```bash
$ git clone git@gitlab.com:dalax/inhalt-design-system.git
$ cd inhalt-design-system
$ yarn install
```

#### Commands
There are several scripts for controlling the project.
+ `$ yarn compile` - Transpiles all Typescript files and puts them into `./lib` folder.
+ `$ yarn clean` - Deleted `./lib` folder
+ `$ yarn ci` - Runs ESLint check and executes tests.

#### Run Storybook UI
`$ yarn start` - Runs Storybook UI server.


## Commiting
We use [conventional commit](https://www.conventionalcommits.org/) standart for commiting our code.
> **Warning!** \
> Husky validates this process, so all the other unaccepted formattings will be refused from being commited.

`<type>: <description> (TRELLO_TASK_ID)` is a format of commit messages, that was approved by conventional commit standart.
Here are types of commits:
<table>
    <tr>
        <td>build</td>
        <td>Сборка проекта или изменения внешних зависимостей</td>
    </tr>
    <tr>
        <td>ci</td>
        <td>Настройка CI и работа со скриптами</td>
    </tr>
    <tr>
        <td>docs</td>
        <td>Обновление документации</td>
    </tr>
    <tr>
        <td>feat</td>
        <td>Добавление нового функционала</td>
    </tr>
    <tr>
        <td>fix</td>
        <td>Исправление ошибок</td>
    </tr>
    <tr>
        <td>perf</td>
        <td>Изменения направленные на улучшение производительности</td>
    </tr>
    <tr>
        <td>refactor</td>
        <td>Правки кода без исправления ошибок или добавления новых функций</td>
    </tr>
    <tr>
        <td>revert</td>
        <td>Откат на предыдущие коммиты</td>
    </tr>
    <tr>
        <td>style</td>
        <td>Правки по кодстайлу (табы, отступы, точки, запятые и т.д.)</td>
    </tr>
    <tr>
        <td>test</td>
        <td>Добавление тестов</td>
    </tr>
</table>

## Workflow (for new Developers)
Here are the steps of workflow (while starting the task):
1. Pull from master `$ git pull origin master`
2. Create new branch, named by TRELLO_TASK_ID (e.g A-1). `$ git checkout -b TRELLO_TASK_ID`
3. Create your component folder, and develop.
4. Export your component in `src/components/index.ts`
5. Commit changes and push to remote repository.

    ```bash
    $ git add .
    $ git commit -m '<type>: Message (TRELLO_TASK_ID)'
    $ git push origin TRELLO_TASK_ID
    ```
6. Go to [GitLab](gitlab.com), and create Merge Request
    > **Warning!** \
     YOU HAVE TO NAME YOUR MR(MERGE REQUEST) ALSO IN CONVENTIONAL COMMITS FORMATTING.
7. TeamLead will give reviews for your code.
8. Go to page of your MR and check commits that TeamLead made and fix your code.
9. Commit your fixes and push to your branch (See 5th step).
10. PROFIT! Teamlead merges your code, and that's it. Checkout to master and pull updates.
    ```bash
    $ git checkout master
    $ git pull origin master
    ```
