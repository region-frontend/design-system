export const getDays = (dateDifference: number): number =>
  Math.floor(dateDifference / (1000 * 60 * 60 * 24));
export const getHours = (dateDifference: number): number =>
  Math.floor(dateDifference / (1000 * 60 * 60));
export const getMinutes = (dateDifference: number): number =>
  Math.floor(dateDifference / (1000 * 60));
export const getSeconds = (dateDifference: number): number => Math.floor(dateDifference / 1000);
