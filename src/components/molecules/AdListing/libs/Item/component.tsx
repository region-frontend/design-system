import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FC } from 'react';

import { colors, typography } from '../../../../../core';
import { Props } from './props';

export const Item: FC<Props> = ({ title, description, onClick, style, ...rest }: Props) => {
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        cursor: 'pointer',
        padding: '15px 0',
        ...style,
      }}
      onClick={onClick}
      {...rest}
    >
      <div style={{ flex: 1, marginRight: 30, overflow: 'hidden' }}>
        <span
          className={'d-block'}
          style={{
            color: colors.Text.MainBlack,
            fontWeight: 600,
            fontSize: typography.Size.RegularText,
          }}
        >
          {title}
        </span>
        <p
          style={{
            margin: 0,
            marginTop: 5,
            color: colors.Text.GreySecondary,
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            fontSize: typography.Size.RegularText,
          }}
        >
          {description}
        </p>
      </div>
      <div
        style={{
          fontSize: 20,
          color: colors.Text.GreySecondary,
        }}
      >
        <FontAwesomeIcon icon={faAngleRight} />
      </div>
    </div>
  );
};
