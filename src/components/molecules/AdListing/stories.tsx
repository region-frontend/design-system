import { Story } from '@storybook/react';
import React from 'react';

import { AdListing } from './component';
import { AdListingProps } from './index';

export default {
  title: 'molecules/AdListing',
  component: AdListing,
};

export const Appearance: Story<AdListingProps> = (args) => {
  return (
    <>
      <AdListing {...args} />
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  items: [
    {
      title: 'Синий Bentley Continental',
      description:
        'Я хотел бы арендовать на целый день ваш автомобиль. И так много текста бла бла бла',
      onClick: () => alert('Синий Bentley'),
    },
    {
      title: 'Белый Bentley Continental',
      description:
        'Я хотел бы арендовать на целый день ваш автомобиль. И так много текста бла бла бла',
      onClick: () => alert('Белый Bentley'),
    },
  ],
};
