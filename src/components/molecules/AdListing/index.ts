import { ItemProps } from './libs/Item';
import { Props } from './props';

export * from './component';
export { Item as AdListingItem } from './libs/Item';

export type AdListingProps = Props;
export type AdListingItemProps = ItemProps;
