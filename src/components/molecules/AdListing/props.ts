import { HTMLAttributes } from 'react';

import { ItemProps } from './libs/Item';

export type Props = Omit<HTMLAttributes<HTMLUListElement>, 'children'> & {
  readonly items: ReadonlyArray<ItemProps>;
};
