import React, { FC } from 'react';

import { colors } from '../../../core';
import { Divider } from '../../atoms';
import { Item } from './libs/Item';
import { Props } from './props';

export const AdListing: FC<Props> = ({ items, style, ...rest }: Props) => {
  return (
    <ul
      style={{
        padding: 0,
        listStyle: 'none',
        margin: 0,
        ...style,
      }}
      {...rest}
    >
      {items.map((n, i, arr) => (
        <>
          <Item {...n} key={i} />
          {i !== arr.length - 1 && (
            <Divider color={colors.Text.GreySlight} style={{ opacity: 0.5 }} />
          )}
        </>
      ))}
    </ul>
  );
};
