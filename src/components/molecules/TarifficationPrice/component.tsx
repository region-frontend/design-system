import React, { FC } from 'react';

import { colors } from '../../../core';
import { Props } from './props';

export const TarifficationPrice: FC<Props> = ({
  price,
  currency,
  period,
  color,
  style,
  ...rest
}: Props) => {
  return (
    <div
      style={{
        gap: 3,
        display: 'flex',
        alignItems: 'center',
        ...style,
      }}
      {...rest}
    >
      <span style={{ color: `${color || colors.Brand.MainColor}` }}>{price}</span>
      <div style={{ color: `${color || colors.Brand.MainColor}` }}>
        {currency && currency === 'KZT' ? (
          <span>₸</span>
        ) : currency === 'RUB' ? (
          <span>₽</span>
        ) : currency === 'USD' ? (
          <span>$</span>
        ) : null}
      </div>
      <span style={{ color: `${color || null}` }}>/</span>
      <span style={{ color: `${color || null}`, fontSize: 14, fontWeight: 400 }}>{period}</span>
    </div>
  );
};
