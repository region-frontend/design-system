import { Story } from '@storybook/react';
import React from 'react';

import { TarifficationPrice } from './component';
import { TarifficationPriceProps } from './index';

export default {
  title: 'molecules/TarifficationPrice',
  component: TarifficationPrice,
};

export const Appearance: Story<TarifficationPriceProps> = () => {
  return (
    <>
      <TarifficationPrice color={'#d74646'} period={'день'} currency={'KZT'} price={280} />
    </>
  );
};
