import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly price: number;
  readonly currency: 'KZT' | 'RUB' | 'USD';
  readonly period: string;
  readonly color?: string;
};
