import React, { FC } from 'react';

import { colors, typography } from '../../../core';
import { Avatar, Card, RatingStars, Typography } from '../../atoms';
import { Props } from './props';

export const UserRating: FC<Props> = ({ user, rating, style, comment, ...rest }: Props) => {
  const { size: ratingSize, ...ratingRest } = rating;
  return (
    <Card
      style={{
        display: 'flex',
        alignItems: 'flex-start',
        gap: 20,
        padding: 20,
        ...style,
      }}
      borderRadius={10}
      {...rest}
    >
      <div
        style={{
          flex: `0 0 ${user.avatar.size || '50px'}`,
        }}
      >
        <Avatar url={user.avatar.url} />
      </div>
      <div>
        <Typography
          as={'span'}
          size={typography.Size.RegularText}
          style={{
            display: 'block',
            color: colors.Text.MainBlack,
            marginBottom: 9,
            fontWeight: 400,
          }}
        >
          {user.name}
        </Typography>
        <div className={'d-flex align-items-center'}>
          <Typography
            className={'mr-3'}
            as={'span'}
            size={typography.Size.RegularText}
            style={{
              fontWeight: 400,
            }}
          >
            {ratingRest.score?.toFixed(1)}
          </Typography>
          <RatingStars size={ratingSize || 13} {...ratingRest} />
        </div>
        {comment ? (
          <Typography
            as={'p'}
            size={typography.Size.RegularText}
            style={{
              color: colors.Text.DarkGrey,
            }}
          >
            {comment}
          </Typography>
        ) : null}
      </div>
    </Card>
  );
};
