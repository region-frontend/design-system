import { Story } from '@storybook/react';
import React from 'react';

import { UserRating } from './component';
import { UserRatingProps } from './index';

export default {
  title: 'organisms/UserRating',
  component: UserRating,
};

export const Appearance: Story<UserRatingProps> = (args) => {
  return <UserRating {...args} />;
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  user: {
    avatar: {
      url:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png',
    },
    name: 'Асет Салауатов',
  },
  rating: {
    score: 3,
    maxScore: 5,
  },
  comment:
    'Супер авто!Доволен на 100%.За эти деньги альтернативы нет.Очень быстое и мега комфортное авто. Общее впечатление положительное. Но жестковат, что то типа бехи в поведении и драйве на дороге. Для него нужен идеальный автобан, тогда он срывается с места и мчится как зверь. При этом жрет бензин немеряно.',
};
