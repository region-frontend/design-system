import { CardProps, RatingStarsProps } from '../../atoms';

export type Props = CardProps & {
  readonly user: {
    readonly avatar: {
      readonly url: string;
      readonly size?: string;
    };
    readonly name: string;
    readonly ratingSize?: RatingStarsProps['size'];
  };
  readonly rating: Pick<RatingStarsProps, 'score' | 'maxScore' | 'size' | 'disabled'>;
  readonly comment?: string;
};
