import React, { FC } from 'react';

import { colors } from '../../../core';
import { Card, ImageCard } from '../../atoms';
import { TarifficationPrice } from '../../molecules';
import { Props } from './props';

export const AdCard: FC<Props> = ({
  title,
  location,
  src,
  period,
  currency,
  price,
  ...rest
}: Props) => {
  return (
    <Card
      style={{
        gap: 10,
      }}
      {...rest}
    >
      <ImageCard src={src} />
      <div
        style={{ color: `${colors.Text.MainBlack}`, fontWeight: 500, fontSize: 16, marginTop: 15 }}
      >
        {title}
      </div>
      <div style={{ fontSize: 14, color: `${colors.Text.GreySecondary}`, marginTop: 5 }}>
        {location}
      </div>
      <TarifficationPrice
        style={{ marginTop: 10 }}
        price={price}
        currency={currency}
        period={period}
      />
    </Card>
  );
};
