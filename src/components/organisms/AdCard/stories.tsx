import { Story } from '@storybook/react';
import React from 'react';

import { StorybookAppearanceTypes } from '../../../core/storybook';
import { AdCard } from './component';
import { AdCardProps } from './index';

export default {
  title: 'organisms/AdCard',
  component: AdCard,
  argTypes: {
    borderSize: {
      description: 'Size of border (px)',
      table: {
        category: StorybookAppearanceTypes.Border,
        defaultValue: { summary: 1 },
      },
      control: 'number',
    },
    borderRadius: {
      description: 'Radius of border (px)',
      table: {
        category: StorybookAppearanceTypes.Border,
        defaultValue: { summary: 4 },
      },
      control: 'number',
    },
    disableShadow: {
      description: 'Whether component should have shadow or not',
      table: {
        defaultValue: { summary: false },
      },
      control: 'boolean',
    },
  },
};

export const Appearance: Story<AdCardProps> = () => {
  return (
    <AdCard
      className={'py-3 px-4'}
      title={'Аренда Lexus GS 350 с водителем'}
      location={'Алматы, Алатауский район'}
      price={280}
      currency={'KZT'}
      period={'day'}
      src={'https://www.focus2move.com/wp-content/uploads/2020/08/Tesla-Roadster-2020-1024-03.jpg'}
    />
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  disableShadow: false,
  borderRadius: 4,
  borderSize: 1,
};
