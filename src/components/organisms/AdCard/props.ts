import { HTMLAttributes } from 'react';

import { CardProps, ImageCardProps } from '../../atoms';
import { TarifficationPriceProps } from '../../molecules';

export type Props = HTMLAttributes<HTMLDivElement> &
  CardProps &
  TarifficationPriceProps &
  ImageCardProps & {
    readonly title?: string;
    readonly location?: string;
  };
