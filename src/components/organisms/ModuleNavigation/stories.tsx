import { faBox, faComment, faHeart, faHome, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Story } from '@storybook/react';
import React from 'react';

import { ModuleNavigation, ModuleNavigationProps } from './index';

export default {
  title: 'organisms/Navigation/ModuleNavigation',
  component: ModuleNavigation,
};

export const Appearance: Story<ModuleNavigationProps> = (args) => {
  return (
    <>
      <ModuleNavigation {...args} />
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  centerItem: {
    icon: <FontAwesomeIcon icon={faPlus} />,
    onClick: () => alert('Plus clicked'),
  },
  leftItems: [
    {
      icon: <FontAwesomeIcon icon={faHome} />,
      label: 'Главная',
      isActive: true,
      onClick: () => alert('Главная clicked'),
    },
    {
      icon: <FontAwesomeIcon icon={faHeart} />,
      label: 'Избранные',
      onClick: () => alert('Избранные clicked'),
    },
  ],
  rightItems: [
    {
      icon: <FontAwesomeIcon icon={faBox} />,
      label: 'Мои объявления',
      onClick: () => alert('Мои объявления clicked'),
    },
    {
      icon: <FontAwesomeIcon icon={faComment} />,
      label: 'Сообщения',
      onClick: () => alert('Сообщения clicked'),
    },
  ],
};
