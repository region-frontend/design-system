import { CardProps } from '../../atoms';
import { ItemProps } from './libs/Item';

export type Props = CardProps & {
  readonly leftItems?: ReadonlyArray<ItemProps>;
  readonly rightItems?: ReadonlyArray<ItemProps>;
  readonly centerItem: ItemProps;
  readonly containerWidth?: number;
};
