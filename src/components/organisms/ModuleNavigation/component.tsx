import React, { FC } from 'react';

import { colors } from '../../../core';
import { Card, Container } from '../../atoms';
import { Item } from './libs/Item';
import { Props } from './props';

export const ModuleNavigation: FC<Props> = ({
  leftItems,
  rightItems,
  centerItem,
  containerWidth,
  ...rest
}: Props) => {
  const Wrapper = containerWidth ? Container : 'div';
  return (
    <Card background={colors.Background.White} {...rest}>
      <Wrapper
        style={{
          display: 'flex',
          justifyContent: 'space-between',
        }}
      >
        <div style={{ flex: '0 0 40%', display: 'flex', justifyContent: 'space-around' }}>
          {leftItems?.length ? leftItems.map((n, i) => <Item key={i} {...n} />) : null}
        </div>
        <div style={{ flex: 1, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Item center {...centerItem} />
        </div>
        <div style={{ flex: '0 0 40%', display: 'flex', justifyContent: 'space-around' }}>
          {rightItems?.length ? rightItems.map((n, i) => <Item key={i} {...n} />) : null}
        </div>
      </Wrapper>
    </Card>
  );
};
