/* @jsx jsx */
import { jsx } from '@emotion/core';
import { FC } from 'react';

import { colors } from '../../../../../core';
import { CircleButton } from '../../../../atoms';
import { Props } from './props';

export const Item: FC<Props> = ({ label, icon, center, style, isActive, ...rest }: Props) => {
  return center ? (
    <CircleButton
      appearance={'default'}
      size={50}
      style={{
        fontSize: 16,
        zIndex: 999,
        transform: 'translateY(-25px)',
        ...style,
      }}
      {...rest}
    >
      {icon}
    </CircleButton>
  ) : (
    <button
      css={{
        color: center || isActive ? colors.Brand.MainColor : colors.Text.GreySecondary,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        transition: 'color 0.1s',
        background: colors.Background.Transparent,
        padding: '10px 10px',
        border: 'none',
        cursor: 'pointer',
        zIndex: 999,
        '&:hover': {
          color: colors.Brand.MainColor,
        },
        ...style,
      }}
      {...rest}
    >
      <div
        style={{
          textAlign: 'center',
          width: '100%',
          fontSize: 18,
          marginBottom: label ? 5 : 0,
        }}
      >
        {icon}
      </div>
      {label ? <div style={{ fontSize: 11 }}>{label}</div> : null}
    </button>
  );
};
