import { ButtonHTMLAttributes, MouseEvent, ReactNode } from 'react';

export type Props = ButtonHTMLAttributes<HTMLButtonElement> & {
  readonly onClick?: (e: MouseEvent<HTMLButtonElement>) => void;
  readonly label?: string;
  readonly icon: ReactNode;
  readonly isActive?: boolean;
  readonly center?: boolean;
};
