import { ItemProps } from './libs/Item';
import { Props } from './props';

export * from './component';

export type ModuleNavigationProps = Props;

export * as ModuleNavigationItem from './libs/Item';

export type ModuleNavigationItemProps = ItemProps;
