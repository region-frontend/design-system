import { faClock } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import React, { FC } from 'react';

import { colors, typography } from '../../../core';
import { getDays } from '../../../core/utils';
import { Card, CardHeader, Timer, Typography } from '../../atoms';
import { Props } from './props';

export const BoostCard: FC<Props> = ({ expirationDate, title, className, ...rest }: Props) => {
  const daysDifference = expirationDate
    ? Math.abs(getDays(new Date(Date.now()).getTime() - expirationDate.getTime()))
    : null;
  return (
    <Card className={classNames(className, 'p-4')} {...rest}>
      <CardHeader
        style={{
          flexDirection: 'row-reverse',
        }}
        title={
          <Typography
            as={'span'}
            size={typography.Size.RegularHeading}
            style={{
              display: 'block',
              color: colors.Brand.MainColor,
              flex: 1,
            }}
          >
            {title}
          </Typography>
        }
        button={
          daysDifference ? (
            <div
              style={{
                color: colors.Text.GreySecondary,
                fontSize: typography.Size.RegularText,
              }}
            >
              <FontAwesomeIcon icon={faClock} className={'mr-2'} />
              <span>{daysDifference} дня</span>
            </div>
          ) : null
        }
        className={'mb-3'}
      />
      <span
        style={{
          color: colors.Text.GreySecondary,
          marginBottom: 5,
        }}
      >
        До окончания осталось
      </span>
      {expirationDate && (
        <Timer
          style={{
            color: colors.Text.MainBlack,
            fontSize: typography.Size.LargeHeading,
          }}
          pivotDate={expirationDate}
        />
      )}
    </Card>
  );
};
