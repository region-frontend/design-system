import { Story } from '@storybook/react';
import React from 'react';

import { BoostCard } from './component';
import { BoostCardProps } from './index';

export default {
  title: 'organisms/BoostCard',
  component: BoostCard,
};

export const Appearance: Story<BoostCardProps> = (args) => {
  return (
    <>
      <BoostCard {...args} />
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  expirationDate: new Date(2021, 9, 26),
  title: 'Топ-объявление',
};
