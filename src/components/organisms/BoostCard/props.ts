import { CardProps } from '../../atoms';

export type Props = Omit<CardProps, 'children'> & {
  readonly expirationDate?: Date;
  readonly title: string;
};
