import { ModalProps } from '../../atoms';

export type Props = Omit<ModalProps, 'onChange' | 'title'> & {
  readonly onChange?: (score: number, comment: string) => void;
  readonly title: string;
};
