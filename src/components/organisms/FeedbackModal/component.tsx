import React, { FC, FormEvent, useState } from 'react';

import { colors, typography } from '../../../core';
import { Button, Field, Modal, RatingStars, Typography } from '../../atoms';
import { Props } from './props';

export const FeedbackModal: FC<Props> = ({ onChange, title, ...rest }: Props) => {
  const [score, setScore] = useState(0);
  const [comment, setComment] = useState('');
  const handleScore = (scoreArg: number): void => {
    setScore(scoreArg);
  };
  const handleSave = (): void => {
    onChange ? onChange(score, comment) : void 0;
  };

  return (
    <Modal
      cardProps={{
        className: 'p-4',
        style: { background: colors.Background.White, borderRadius: 15 },
      }}
      {...rest}
    >
      <Typography
        as={'h1'}
        size={typography.Size.LargeHeading}
        style={{ textAlign: 'center', fontWeight: 400 }}
      >
        {title}
      </Typography>
      <RatingStars
        maxScore={5}
        onChange={handleScore}
        disabled={score > 0}
        score={score}
        size={typography.Size.LargeHeading}
        style={{
          justifyContent: 'center',
          gap: 15,
        }}
      />
      <Field
        wrapperProps={{
          className: 'mb-3 mt-3',
        }}
        type={'textarea'}
        onChange={(e: FormEvent<HTMLTextAreaElement>) => setComment(e.currentTarget.value)}
        label={'Оставьте отзыв про объявления'}
        style={{
          fontSize: typography.Size.LargeText,
        }}
      />
      <Button
        style={{
          fontSize: typography.Size.LargeText,
          borderRadius: 10,
          padding: 15,
        }}
        appearance={'primary'}
        onClick={handleSave}
        block
      >
        Оценить
      </Button>
    </Modal>
  );
};
