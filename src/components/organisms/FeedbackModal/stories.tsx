import { Story } from '@storybook/react';
import React, { useState } from 'react';

import { FeedbackModal } from './component';
import { FeedbackModalProps } from './index';

export default {
  title: 'organisms/FeedbackModal',
  component: FeedbackModal,
};

export const Appearance: Story<FeedbackModalProps> = () => {
  const [isOpen, setIsOpen] = useState(false);
  const handleSave = (score: number, comment: string): void => {
    alert(`Score: ${score}\nComment: ${comment}`);
  };
  return (
    <>
      <button onClick={() => setIsOpen((prev) => !prev)}>Open Modal</button>
      <FeedbackModal
        title={'Оцените объявления'}
        active={isOpen}
        onClose={() => setIsOpen(false)}
        onChange={handleSave}
      />
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {};
