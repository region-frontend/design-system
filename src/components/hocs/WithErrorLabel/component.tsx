import React, { FC } from 'react';

import { colors, typography } from '../../../core';
import { Props } from './props';

export const WithErrorLabel: FC<Props> = ({ message, children, ...rest }: Props) => {
  return (
    <div {...rest}>
      {children}
      <span
        className={'mt-2'}
        style={{
          display: 'block',
          color: colors.Brand.Error,
          fontWeight: 400,
          fontSize: typography.Size.SmallText,
        }}
      >
        {message}
      </span>
    </div>
  );
};
