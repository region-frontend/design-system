import { Story } from '@storybook/react';
import React, { FormEvent, useState } from 'react';

import { Field } from '../../atoms';
import { WithErrorLabel } from './component';
import { WithErrorLabelProps } from './index';

export default {
  title: 'hoc/WithErrorLabel',
  component: WithErrorLabel,
};

export const Appearance: Story<WithErrorLabelProps> = () => {
  const [errorMessage, setErrorMessage] = useState<string | undefined>();
  const handleChange = (e: FormEvent<HTMLInputElement>): void => {
    if (e.currentTarget.value.length > 6) {
      setErrorMessage(undefined);
    } else {
      setErrorMessage('Text must be more than 6');
    }
  };

  return (
    <WithErrorLabel message={errorMessage}>
      <Field label={'Text'} onChange={handleChange} type={'text'} />
    </WithErrorLabel>
  );
};
