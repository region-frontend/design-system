import { Story } from '@storybook/react';
import React from 'react';

import { CardHeader } from './component';
import { CardHeaderProps } from './index';

export default {
  title: 'atoms/Cards/CardHeader',
  component: CardHeader,
};

export const Appearance: Story<CardHeaderProps> = () => {
  return (
    <>
      <CardHeader
        button={
          <button
            style={{
              fontSize: 20,
              background: 'transparent',
              border: 'none',
            }}
          >
            🛈
          </button>
        }
        title={'Рекламировать обьявление'}
      />

      <CardHeader title={'Promotion'} />
    </>
  );
};
