import styled from '@emotion/styled';
import React, { FC } from 'react';

import { Props } from './props';

const CardHeaderBase: FC<Props> = ({ button, title, style, ...rest }: Props) => {
  return (
    <div
      style={{
        gap: 10,
        ...style,
      }}
      {...rest}
    >
      {button && <div>{button}</div>}
      {typeof title === 'string' ? (
        <div
          style={{
            flex: 1,
            fontSize: 20,
            fontWeight: 'bold',
          }}
        >
          {title}
        </div>
      ) : (
        title
      )}
    </div>
  );
};

export const CardHeader = styled(CardHeaderBase)<Props>`
  display: flex;
  align-items: center;
`;
