import { HTMLAttributes, ReactNode } from 'react';

export type Props = Omit<HTMLAttributes<HTMLDivElement>, 'title'> & {
  readonly button?: ReactNode;
  readonly title?: ReactNode;
};
