import { HTMLAttributes } from 'react';

export type Props = Omit<HTMLAttributes<HTMLDivElement>, 'onChange'> & {
  readonly score?: number;
  readonly maxScore?: number;
  readonly size?: number;
  readonly onChange?: (score: number) => void;
  readonly disabled?: boolean;
};
