/* @jsx jsx */
import { jsx } from '@emotion/core';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FC, Fragment, useState } from 'react';

import { colors } from '../../../core';
import { emptyArrayGenerator } from '../../../core/utils';
import { Props } from './props';

export const RatingStars: FC<Props> = ({
  score = 0,
  maxScore = 5,
  size = 14,
  style,
  disabled,
  onChange,
  ...rest
}: Props) => {
  const [hoveredStar, setHoveredStar] = useState(0);
  const activeStars = Math.floor(score);
  const inactiveStars = maxScore - Math.ceil(score);
  const leftStarPart = score - activeStars;

  const handleClick = (scoreArg: number) => () => {
    onChange ? onChange(scoreArg) : void 0;
  };

  const handleStarHover = (index: number) => () => {
    setHoveredStar(index);
  };

  const handleStarHoverOut = (): void => {
    setHoveredStar(0);
  };

  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        gap: 2,
        fontSize: size,
        ...style,
      }}
      {...rest}
    >
      {disabled ? (
        <Fragment>
          {emptyArrayGenerator(activeStars).map((i) => (
            <FontAwesomeIcon key={i} icon={faStar} style={{ color: colors.Brand.Yellow }} />
          ))}
          {leftStarPart ? (
            <div
              style={{
                position: 'relative',
                display: 'flex',
                alignItems: 'center',
              }}
              key={emptyArrayGenerator(activeStars).length}
            >
              <FontAwesomeIcon
                style={{
                  color: colors.Brand.Yellow,
                }}
                icon={faStar}
              />
              <FontAwesomeIcon
                style={{
                  WebkitMaskImage: `linear-gradient(to right, transparent ${
                    leftStarPart * 100
                  }%, black 0)`,
                  color: colors.Text.GreySecondary,
                  position: 'absolute',
                  left: 0,
                  top: 0,
                }}
                icon={faStar}
              />
            </div>
          ) : null}
          {emptyArrayGenerator(inactiveStars).map((i) => (
            <FontAwesomeIcon
              key={activeStars + i}
              icon={faStar}
              style={{ color: colors.Text.GreySecondary }}
            />
          ))}
        </Fragment>
      ) : (
        <Fragment>
          {emptyArrayGenerator(maxScore).map((i) => (
            <FontAwesomeIcon
              key={i}
              icon={faStar}
              onMouseOver={handleStarHover(i + 1)}
              onMouseOut={handleStarHoverOut}
              css={{
                color: i < hoveredStar ? colors.Brand.Yellow : colors.Text.GreySecondary,
                cursor: 'pointer',
                transition: 'transform 0.2s',
                '&:hover': {
                  transform: 'scale(1.1)',
                },
              }}
              onClick={handleClick(i + 1)}
            />
          ))}
        </Fragment>
      )}
    </div>
  );
};
