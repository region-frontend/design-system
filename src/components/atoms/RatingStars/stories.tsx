import { Story } from '@storybook/react';
import React, { useState } from 'react';

import { RatingStars } from './component';
import { RatingStarsProps } from './index';

export default {
  title: 'atoms/Other/RatingStars',
  component: RatingStars,
  argTypes: {
    score: {
      control: 'number',
    },
    maxScore: {
      control: 'number',
    },
    size: {
      control: 'number',
    },
  },
};

export const Appearance: Story<RatingStarsProps> = (args) => {
  const [score, setScore] = useState(3);
  const [disabled, setDisabled] = useState(false);

  const onChange = (scoreArg: number): void => {
    setScore((score + scoreArg) / 2);
    setDisabled(true);
  };

  return (
    <>
      <RatingStars onChange={onChange} disabled={disabled} score={score} {...args} />
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  maxScore: 5,
  size: 14,
};
