import { Story } from '@storybook/react';
import React from 'react';

import { Spinner } from './component';
import { SpinnerProps } from './index';

export default {
  title: 'atoms/Other/Spinner',
  component: Spinner,
  argTypes: {
    fill: {
      options: [25, 50, 75],
      control: 'select',
    },
    size: {
      control: 'number',
    },
    diameter: {
      control: 'number',
    },
    speed: {
      control: 'number',
    },
  },
};

export const Appearance: Story<SpinnerProps> = (args) => {
  return (
    <>
      <Spinner {...args} />
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  fill: 25,
  speed: 0.8,
  size: 2,
  diameter: 25,
};
