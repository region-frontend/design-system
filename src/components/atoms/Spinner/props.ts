import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly color?: string;
  readonly fill?: 25 | 50 | 75;
  readonly speed?: number;
  readonly size?: number;
  readonly diameter?: number;
};
