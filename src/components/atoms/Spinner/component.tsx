import { css } from '@emotion/core';
import styled from '@emotion/styled';

import { colors } from '../../../core';
import { Props } from './props';

export const Spinner = styled.div<Props>`
  ${({ color, fill = 25, speed = 0.8, size = 2, diameter = 25 }) => css`
    display: inline-block;
    border: ${size}px solid ${colors.Background.Transparent};
    border-top: ${size}px solid ${color};
    border-right: ${size}px solid
      ${[50, 75].includes(fill) ? colors.Brand.MainColor : colors.Background.Transparent};
    border-bottom: ${size}px solid
      ${fill === 75 ? colors.Brand.MainColor : colors.Background.Transparent};
    border-radius: 50%;
    width: ${diameter}px;
    height: ${diameter}px;
    animation: spin ${speed}s linear infinite;

    @keyframes spin {
      0% {
        transform: rotate(0deg);
      }
      100% {
        transform: rotate(360deg);
      }
    }
  `}
`;

Spinner.defaultProps = {
  color: colors.Brand.MainColor,
};
