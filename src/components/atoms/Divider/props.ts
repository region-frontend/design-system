import { HTMLAttributes } from 'react';

export type Props = Omit<HTMLAttributes<HTMLHRElement>, 'height'> & {
  readonly appearance?: 'solid' | 'dashed' | 'dotted';
  readonly size?: number;
  readonly color?: string;
};
