import { Story } from '@storybook/react';
import React from 'react';

import { Divider } from './component';
import { DividerProps } from './index';

export default {
  title: 'atoms/Layout/Divider',
  component: Divider,
};

export const Appearance: Story<DividerProps> = (args) => {
  return (
    <>
      <Divider {...args} />
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  appearance: 'solid',
  color: 'grey',
  size: 1,
};
