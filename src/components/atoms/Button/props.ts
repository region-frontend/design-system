import { ButtonHTMLAttributes } from 'react';

export type Props = ButtonHTMLAttributes<HTMLButtonElement> & {
  readonly appearance?: 'default' | 'primary' | 'translucent' | 'text' | 'link';
  readonly loading?: boolean;
  readonly rounded?: boolean;
  readonly block?: boolean;
};
