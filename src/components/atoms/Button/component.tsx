import { css } from '@emotion/core';
import styled from '@emotion/styled';
import React, { FC } from 'react';

import { colors } from '../../../core';
import { Loading } from '../Loading';
import { Props } from './props';

const ButtonBase: FC<Props> = ({ children, loading, ...rest }: Props) => (
  <button {...rest}>{loading ? <Loading /> : children}</button>
);

export const Button = styled(ButtonBase)<Props>`
  ${({ appearance, rounded, disabled, block }) =>
    css`
      display: inline-block;
      border-radius: ${rounded ? 100 : 4}px;
      border: none;
      cursor: pointer;
      font-size: 13px;
      font-family: inherit;
      font-weight: 600;
      padding: 8px 15px;
      transition: border-color 0.3s, color 0.3s, background 0.3s;
      width: ${block ? `100%` : `initial`};
      ${disabled
        ? css`
            color: ${colors.Brand.Violet14};
            background: ${colors.Brand.Violet17};
          `
        : css`
        ${
          appearance === 'primary' &&
          css`
            background: ${colors.Brand.MainColor};
            color: ${colors.Text.Light};
            border: 1px solid ${colors.Brand.MainColor};
            &:hover {
              border-color: ${colors.Brand.MainColorDark};
              background: ${colors.Brand.MainColorDark};
            }
          `
        }
        ${
          appearance === 'default' &&
          css`
            border: 1px solid ${colors.Brand.MainColor};
            background: ${colors.Background.White};
            color: ${colors.Brand.MainColor};
            &:hover {
              border-color: ${colors.Brand.MainColorDark};
              color: ${colors.Brand.MainColorDark};
            }
          `
        }
        ${
          appearance === 'translucent' &&
          css`
            background: ${colors.Brand.Violet17};
            color: ${colors.Brand.MainColor};
            &:hover {
              color: ${colors.Brand.MainColorDark};
            }
          `
        }
        ${
          appearance === 'text' &&
          css`
            background: ${colors.Background.Transparent};
            color: ${colors.Text.MainBlack};
          `
        }
        ${
          appearance === 'link' &&
          css`
            background: ${colors.Background.Transparent};
            color: ${colors.Brand.MainColor};
            &:hover {
              text-decoration: underline;
            }
            &:active {
              color: ${colors.Brand.MainColorDark};
            }
          `
        }
      `}
    `}
`;

Button.defaultProps = {
  appearance: 'default',
};
