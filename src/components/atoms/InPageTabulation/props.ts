import { HTMLAttributes } from 'react';

export type Props = Omit<HTMLAttributes<HTMLDivElement>, 'children' | 'onChange'> & {
  readonly items: ReadonlyArray<{
    readonly label: string;
    readonly index: string;
  }>;
  readonly currentTab?: string;
  readonly onChange?: (activeTab: string) => void;
};
