import { Story } from '@storybook/react';
import React, { useState } from 'react';

import { InPageTabulation } from './component';
import { InPageTabulationProps } from './index';

export default {
  title: 'atoms/Navigation/InPageTabulation',
  component: InPageTabulation,
};

export const Appearance: Story<InPageTabulationProps> = () => {
  const items = [
    {
      label: 'Contact',
      index: 'contact',
    },
    {
      label: 'Profile',
      index: 'profile',
    },
    {
      label: 'Home',
      index: 'home',
    },
  ];

  const [currentTab, setCurrentTab] = useState(items[0].index);

  return (
    <>
      <InPageTabulation
        className={'mb-4'}
        items={items}
        onChange={setCurrentTab}
        currentTab={currentTab}
      />
      <span>Current active tab: {currentTab}</span>
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {};
