import React, { FC } from 'react';

import { colors } from '../../../core';
import { Props } from './props';

export const InPageTabulation: FC<Props> = ({
  items,
  onChange,
  currentTab,
  style,
  ...rest
}: Props) => {
  const handleClick = (index: string) => () => {
    onChange ? onChange(index) : void 0;
  };

  return (
    <div
      style={{
        display: 'flex',
        background: colors.Brand.Violet17,
        borderRadius: 4,
        ...style,
      }}
      {...rest}
    >
      {items.map((n) => (
        <button
          key={n.index}
          onClick={handleClick(n.index)}
          style={{
            fontSize: 13,
            display: 'block',
            flex: 1,
            background: currentTab === n.index ? colors.Background.White : 'transparent',
            border: 'none',
            cursor: 'pointer',
            transition: 'transform 0.2s',
            ...(currentTab === n.index
              ? {
                  transform: 'scale(1.1)',
                  color: colors.Brand.MainColor,
                  borderRadius: 10,
                  padding: 7,
                  fontWeight: 600,
                  boxShadow: `0 1px 9px rgba(0,0,0,.1)`,
                }
              : {
                  color: colors.Text.GreySecondary,
                }),
          }}
        >
          {n.label}
        </button>
      ))}
    </div>
  );
};
