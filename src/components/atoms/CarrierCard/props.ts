import { CardProps } from '../Card';

export type Props = Omit<CardProps, 'disableShadow' | 'borderSize'>;
