import React, { FC } from 'react';

import { colors } from '../../../core';
import { Card } from '../Card';
import { Props } from './props';

export const CarrierCard: FC<Props> = ({
  children,
  borderRadius = 10,
  background,
  style,
  ...rest
}: Props) => {
  return (
    <div
      style={{
        ...style,
      }}
    >
      <div
        style={{
          padding: 15,
          background,
          borderRadius: `${borderRadius}px ${borderRadius}px 0 0`,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-around',
          marginBottom: 0,
        }}
      >
        <div
          style={{
            height: 5,
            width: 30,
            background: colors.Text.GreySecondary,
            borderRadius: 10,
          }}
        />
      </div>
      <Card borderRadius={0} disableShadow borderSize={0} background={background} {...rest}>
        {children}
      </Card>
    </div>
  );
};
