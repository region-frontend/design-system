import { Story } from '@storybook/react';
import React from 'react';

import { StorybookAppearanceTypes } from '../../../core/storybook';
import { CarrierCard } from './component';
import { CarrierCardProps } from './index';

export default {
  title: 'atoms/Cards/CarrierCard',
  component: CarrierCard,
  argTypes: {
    borderRadius: {
      description: 'Radius of border (px)',
      table: {
        category: StorybookAppearanceTypes.Border,
        defaultValue: { summary: 4 },
      },
      control: 'number',
    },
  },
};

export const Appearance: Story<CarrierCardProps> = (args) => {
  return (
    <CarrierCard className={'py-3 px-4'} {...args}>
      <p>Lorem ipsum dolor sit amet</p>
    </CarrierCard>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  borderRadius: 10,
};
