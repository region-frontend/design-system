import classNames from 'classnames';
import React, { ChangeEvent, FC, useState } from 'react';

import { SwitchInput } from './libs/SwitchInput';
import { Toggle } from './libs/Toggle';
import { Props } from './props';

export const Switch: FC<Props> = ({ active, disabled, onChange, ...rest }: Props) => {
  const [isActive, setIsActive] = useState(active || false);
  const handleChange = (e: ChangeEvent<HTMLInputElement>): void => {
    setIsActive(e.currentTarget.checked);
    onChange ? onChange(e) : void 0;
  };
  return (
    <>
      <label
        style={{
          position: 'relative',
        }}
      >
        <SwitchInput
          className={classNames('m-0')}
          type={'checkbox'}
          checked={isActive}
          onChange={handleChange}
          disabled={disabled}
          {...rest}
        />
        <Toggle />
      </label>
    </>
  );
};
