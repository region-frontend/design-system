import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLInputElement> & {
  readonly active?: boolean;
  readonly disabled?: boolean;
};
