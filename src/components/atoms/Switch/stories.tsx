import { Story } from '@storybook/react';
import React, { ChangeEvent, useState } from 'react';

import { Switch } from './component';
import { SwitchProps } from './index';

export default {
  title: 'atoms/Form/Switch',
  component: Switch,
  argTypes: {
    active: {
      description: 'Is checkbox active',
      control: 'boolean',
    },
    disabled: {
      description: 'Disabled',
      control: 'boolean',
    },
  },
};

export const Appearance: Story<SwitchProps> = (args) => {
  const [value, setValue] = useState(true);
  return (
    <>
      <Switch
        onChange={(e: ChangeEvent<HTMLInputElement>) => setValue(e.currentTarget.checked)}
        active={value}
        {...args}
      />

      <span>Value: {value ? 'Toggled' : 'Not toggled'}</span>
    </>
  );
};
