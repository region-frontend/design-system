import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLInputElement> & {
  readonly disabled?: boolean;
};
