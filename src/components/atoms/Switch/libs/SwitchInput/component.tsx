import { css } from '@emotion/core';
import styled from '@emotion/styled';

import { colors } from '../../../../../core';
import { Props } from './props';

export const SwitchInput = styled.input<Props>`
  ${({ disabled }) =>
    css`
      position: absolute;
      display: none;
      visibility: hidden;
      &:checked + span {
        background-color: ${colors.Brand.MainColor};
        &:before {
          left: 13px;
        }
      }

      ${disabled
        ? css`
            background-color: ${colors.Text.GreySecondary};
            span {
              background-color: ${colors.Brand.Yellow};
            }
          `
        : css``}
    `}
`;
