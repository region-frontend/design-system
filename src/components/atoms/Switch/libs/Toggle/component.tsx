import styled from '@emotion/styled';

import { colors } from '../../../../../core';

export const Toggle = styled.span`
  display: flex;
  cursor: pointer;
  border-radius: 15px;
  width: 30px;
  height: 19px;
  background-color: ${colors.Text.GreySlight};
  position: relative;
  transition: background-color 0.3s;

  &:before {
    content: '';
    position: absolute;
    top: 2px;
    left: 2px;
    width: 15px;
    height: 15px;
    border-radius: 100%;
    background-color: ${colors.Brand.White};
    transition: left 0.2s;
    box-shadow: 0 3px 8px rgba(0, 0, 0, 0.14902);
  }
`;
