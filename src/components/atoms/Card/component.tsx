import { css } from '@emotion/core';
import styled from '@emotion/styled';

import { colors } from '../../../core';
import { Props } from './props';

export const Card = styled.div<Props>`
  ${({ disableShadow, borderSize, borderRadius = 4, background }) =>
    css`
      background: ${background || 'transparent'};
      border-radius: ${borderRadius}px;
      box-shadow: ${disableShadow ? `none` : `0px 0px 15px rgba(75, 70, 150, 0.1)`};
      border: ${borderSize}px solid ${colors.Text.GreySecondary};
      transition: box-shadow 0.2s;
    `}
`;

Card.defaultProps = {
  borderSize: 0,
};
