import { Props } from './props';

export * from './component';

export type ModalProps = Props;
