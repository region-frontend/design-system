import { Story } from '@storybook/react';
import React, { useState } from 'react';

import { Modal } from './component';
import { ModalProps } from './index';

export default {
  title: 'atoms/Modal',
  component: Modal,
};

export const Appearance: Story<ModalProps> = () => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <button onClick={() => setIsOpen((prev) => !prev)}>Open Modal</button>
      <Modal active={isOpen} onClose={() => setIsOpen(false)}>
        <h1>Hello, nigga</h1>
      </Modal>
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {};
