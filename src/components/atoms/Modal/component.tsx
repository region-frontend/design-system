import React, { FC } from 'react';

import { Card } from '../Card';
import { Props } from './props';

export const Modal: FC<Props> = ({
  children,
  onClose,
  active,
  style,
  cardProps = {},
  withButton,
  bodyProps = {},
  ...rest
}: Props) => {
  const { style: bodyStyles, ...bodyRest } = bodyProps;
  const { style: cardStyles, ...cardRest } = cardProps;
  return (
    <div
      style={{
        ...(active
          ? {
              position: 'fixed',
              zIndex: 9999999,
              left: 0,
              top: 0,
              width: '100%',
              height: '100vh',
            }
          : {
              display: 'none',
            }),
        ...style,
      }}
      {...rest}
    >
      <div
        style={{
          zIndex: 9999999,
          position: 'relative',
          maxWidth: 900,
          margin: 'auto',
          marginTop: 100,
          ...bodyStyles,
        }}
        {...bodyRest}
      >
        <Card borderRadius={6} style={{ background: '#fff', ...cardStyles }} {...cardRest}>
          {withButton ? (
            <div style={{ textAlign: 'right' }}>
              <button
                onClick={onClose}
                style={{
                  background: 'transparent',
                  border: 'none',
                  fontSize: 20,
                  margin: 10,
                  marginLeft: 'auto',
                }}
              >
                X
              </button>
            </div>
          ) : null}
          {children}
        </Card>
      </div>
      <div
        onClick={onClose}
        style={{
          background: 'rgba(0,0,0,.7)',
          width: '100%',
          cursor: 'pointer',
          height: '100vh',
          position: 'absolute',
          top: 0,
          left: 0,
          zIndex: 999999,
        }}
      />
    </div>
  );
};
