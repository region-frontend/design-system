import { HTMLAttributes, ReactNode } from 'react';

import { CardProps } from '../Card';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly active?: boolean;
  readonly onClose?: () => void;
  readonly children?: ReactNode;
  readonly withButton?: boolean;
  readonly cardProps?: CardProps;
  readonly bodyProps?: HTMLAttributes<HTMLDivElement>;
};
