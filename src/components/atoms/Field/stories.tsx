import { Story } from '@storybook/react';
import React, { FormEvent, useState } from 'react';

import { Field } from './component';
import { FieldProps } from './index';

export default {
  title: 'atoms/Form/Field',
  component: Field,
  argTypes: {
    label: {
      description: 'Label',
      control: 'text',
    },
    placeholder: {
      description: 'Shown when label is undefined',
      control: 'text',
    },
    type: {
      description: 'Field type',
      options: ['text', 'number', 'textarea', 'password'],
      control: 'select',
    },
    wrapperProps: {
      description: 'Field wrapper props',
      type: 'HTMLLabelElement',
    },
    disabled: {
      description: 'Disabled',
      control: 'boolean',
    },
    loading: {
      description: 'Disabled',
      control: 'boolean',
    },
  },
};

export const Appearance: Story<FieldProps> = (args) => {
  const [value, setValue] = useState<string>('nigga');
  return (
    <>
      <Field
        onChange={(e: FormEvent<HTMLInputElement | HTMLTextAreaElement>) =>
          setValue(e.currentTarget.value)
        }
        icon={
          <div className={'my-1'} style={{ fontSize: 20 }}>
            🛈
          </div>
        }
        iconSuffix={
          <div className={'my-1'} style={{ fontSize: 20 }}>
            🛈
          </div>
        }
        borderSize={1}
        value={value}
        {...args}
      />

      <span>Value: {value}</span>
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  label: 'Your label',
  wrapperProps: {
    className: 'my-3',
  },
};
