import { InputProps } from './libs/Input';
import { InputWithLabelProps } from './libs/InputWithLabel';
import { Props } from './props';

export * from './component';
export { Input as FieldInput } from './libs/Input';
export { InputWithLabel as FieldWithLabelInput } from './libs/InputWithLabel';

export type FieldProps = Props;
export type FieldInputProps = InputProps;
export type FieldInputWithLabelProps = InputWithLabelProps;
