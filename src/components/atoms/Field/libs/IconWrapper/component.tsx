import React, { FC } from 'react';

import { colors } from '../../../../../core';
import { Props } from './props';

export const IconWrapper: FC<Props> = ({ active, children, ...rest }: Props) => (
  <div
    style={{
      color: colors.Text.GreySecondary,
      transition: 'color 0.3s',
      ...(active
        ? {
            color: colors.Brand.MainColor,
          }
        : {}),
    }}
    {...rest}
  >
    {children}
  </div>
);
