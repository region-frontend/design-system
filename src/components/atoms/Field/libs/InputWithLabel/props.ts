import { HTMLAttributes } from 'react';

import { InputProps } from '../Input';

export type Props = InputProps & {
  readonly wrapperProps?: HTMLAttributes<HTMLLabelElement>;
  readonly disabled?: boolean;
  readonly loading?: boolean;
  readonly borderSize?: number;
  readonly placeholderSize?: number;
};
