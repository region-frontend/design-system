import React, { FC } from 'react';

import { colors } from '../../../../../core';
import { Loading } from '../../../index';
import { IconWrapper } from '../IconWrapper';
import { Input } from '../Input';
import { Props } from './props';

export const InputWithLabel: FC<Props> = ({
  label,
  type,
  placeholder,
  modified,
  active,
  wrapperProps,
  icon,
  loading,
  borderSize,
  disabled,
  placeholderSize,
  iconSuffix,
  ...rest
}: Props) => {
  return (
    <label
      {...{
        ...wrapperProps,
        style: {
          position: 'relative',
          display: 'block',
          borderBottom: `${borderSize || 2}px solid ${colors.Text.GreySecondary}`,
          transition: `border-color 0.3s`,
          ...(active
            ? {
                borderColor: colors.Brand.MainColor,
              }
            : {
                borderColor: colors.Text.GreySecondary,
              }),
          ...(icon
            ? {
                display: 'flex',
                gap: 10,
                alignItems: 'flex-end',
              }
            : {}),
          ...(wrapperProps?.style || {}),
        },
      }}
    >
      {icon ? <IconWrapper active={active}>{icon}</IconWrapper> : null}
      <div style={{ flex: 1 }}>
        {label ? (
          <span
            style={{
              color: colors.Text.GreySecondary,
              fontSize: placeholderSize || 14,
              top: '30px',
              position: 'relative',
              transition: 'color 0.3s, font-size 0.3s, top 0.3s',
              ...(active
                ? {
                    color: colors.Brand.MainColor,
                    top: 0,
                    fontSize: 11,
                  }
                : {}),
              ...(modified
                ? {
                    fontSize: 11,
                    top: 0,
                  }
                : {}),
            }}
          >
            {loading ? <Loading /> : label}
          </span>
        ) : null}
        <Input type={type} disabled={disabled} {...(label ? {} : { placeholder })} {...rest} />
      </div>
      {iconSuffix ? <IconWrapper active={active}>{iconSuffix}</IconWrapper> : null}
    </label>
  );
};
