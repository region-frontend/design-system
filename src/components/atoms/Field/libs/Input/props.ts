import { HTMLAttributes, ReactNode } from 'react';

export type Props = Omit<HTMLAttributes<HTMLInputElement | HTMLTextAreaElement>, 'type'> & {
  readonly icon?: ReactNode;
  readonly iconSuffix?: ReactNode;
  readonly label?: string;
  readonly type: 'text' | 'number' | 'textarea' | 'password';
  readonly modified?: boolean;
  readonly active?: boolean;
  readonly max?: number;
  readonly value?: string;
};
