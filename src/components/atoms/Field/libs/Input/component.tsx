import { css } from '@emotion/core';
import styled from '@emotion/styled';
import classNames from 'classnames';
import React, { FC } from 'react';

import { colors } from '../../../../../core';
import { Props } from '../../props';

const InputBase: FC<Props> = ({ className, type, max, ...rest }: Props) => {
  const Component = type === 'textarea' ? 'textarea' : 'input';

  return (
    <Component className={classNames(className)} type={type} maxLength={max} max={max} {...rest} />
  );
};

export const Input = styled(InputBase)<Props>`
  ${() => css`
    position: relative;
    z-index: 1;
    min-width: 100%;
    max-width: 100%;
    width: 100% !important;
    font-family: inherit;
    max-height: 300px;
    background: ${colors.Background.Transparent};
    outline: none;
    font-size: 14px;
    flex: 1;
    border: none;
    padding: 10px 0;
    transition: font-size 0.3s;
  `}
`;
