import { InputWithLabelProps } from './libs/InputWithLabel';

export type Props = InputWithLabelProps;
