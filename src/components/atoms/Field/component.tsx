import React, { FC, FormEvent, useEffect, useState } from 'react';

import { InputWithLabel } from './libs/InputWithLabel';
import { Props } from './props';

export const Field: FC<Props> = ({ label, type, onChange, placeholder, ...rest }: Props) => {
  const [value, setInnerValue] = useState(rest.value || '');
  const [focused, setFocused] = useState(false);

  useEffect(() => {
    if ('value' in rest) {
      setInnerValue(rest.value || '');
    }
  }, [rest, rest.value]);

  const handleChange = (e: FormEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    const tmpValue = e.currentTarget.value;
    setInnerValue(tmpValue);
    onChange ? onChange(e) : void 0;
  };

  return (
    <InputWithLabel
      modified={value.length > 0}
      active={focused}
      label={label}
      placeholder={placeholder}
      type={type}
      onFocus={() => setFocused(true)}
      onBlur={() => setFocused(false)}
      onChange={handleChange}
      value={value}
      {...rest}
    />
  );
};
