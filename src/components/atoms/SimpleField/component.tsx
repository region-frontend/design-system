/* @jsx jsx */
import { jsx } from '@emotion/core';
import { FC, useState } from 'react';

import { colors } from '../../../core';
import { Props } from './props';

export const SimpleField: FC<Props> = ({
  icon,
  placeholder,
  wrapperProps,
  style,
  withFocusStyles,
  onFocus,
  onBlur,
  ...rest
}: Props) => {
  const [focused, setFocused] = useState(false);
  return (
    <label
      {...wrapperProps}
      css={{
        display: 'flex',
        alignItems: 'center',
        border: `1px solid ${withFocusStyles ? colors.Text.GreySecondary : colors.Brand.MainColor}`,
        boxSizing: 'border-box',
        borderRadius: '10px',
        padding: '15px',
        transition: 'border-color 0.3s, box-shadow 0.3s, background 0.3s',
        ...(withFocusStyles && focused
          ? {
              borderColor: 'transparent',
              boxShadow: '0 0 10px rgba(0,0,0,.1)',
              background: colors.Background.White,
            }
          : {}),
        ...(wrapperProps?.style || {}),
      }}
    >
      <div style={{ display: 'flex', alignItems: 'center' }}>{icon}</div>
      <input
        placeholder={placeholder}
        style={{
          width: '100%',
          border: 'none',
          outline: 'none',
          fontSize: 14,
          marginLeft: 10,
          ...style,
        }}
        onFocus={(e) => {
          setFocused(true);
          onFocus ? onFocus(e) : void 0;
        }}
        onBlur={(e) => {
          setFocused(false);
          onBlur ? onBlur(e) : void 0;
        }}
        {...rest}
      />
    </label>
  );
};
