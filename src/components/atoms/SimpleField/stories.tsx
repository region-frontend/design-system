import { Story } from '@storybook/react';
import React, { FormEvent, useState } from 'react';

import { SimpleField } from './component';
import { SimpleFieldProps } from './index';

export default {
  title: 'atoms/Form/SimpleField',
  component: SimpleField,
  argTypes: {
    placeholder: {
      description: 'Shown when placeholder is undefined',
      control: 'text',
    },
    disabled: {
      description: 'Disabled',
      control: 'boolean',
    },
    withFocusStyles: {
      description: 'Disabled',
      control: 'boolean',
    },
  },
};

export const Appearance: Story<SimpleFieldProps> = (args) => {
  const [value, setValue] = useState('');
  return (
    <>
      <SimpleField
        onChange={(e: FormEvent<HTMLInputElement | HTMLTextAreaElement>) =>
          setValue(e.currentTarget.value)
        }
        icon={<div style={{ fontSize: 25 }}>🛈</div>}
        placeholder={'Введите название товара/услуги'}
        {...args}
      />

      <span className={'d-block mt-3'}>Value: {value}</span>
    </>
  );
};
