import { HTMLAttributes, InputHTMLAttributes, ReactNode } from 'react';

export type Props = InputHTMLAttributes<HTMLInputElement> & {
  readonly icon?: ReactNode;
  readonly placeholder?: string;
  readonly wrapperProps?: HTMLAttributes<HTMLLabelElement>;
  readonly withFocusStyles?: boolean;
};
