import React, { FC, FormEvent, useState } from 'react';

import { Field } from '../Field';
import { Props } from './props';

export const MaskField: FC<Props> = ({
  mask,
  type,
  label: _a,
  placeholder: _b,
  onChange,
  style,
  contentProps = {},
  ...rest
}: Props) => {
  const [val, setVal] = useState('');
  const maskedChar = '9';

  const { style: contentStyle } = contentProps;

  const formatByMask = (maskTmp: string | undefined, val: string): string => {
    // eslint-disable-next-line functional/no-let
    let count = 0;
    return (
      maskTmp
        ?.split('')
        .map((n) => {
          if (n === maskedChar) {
            return val[count++] || '_';
          }
          return n;
        })
        .join('') || ''
    );
  };

  const handleChange = (e: FormEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    setVal(e.currentTarget.value ? formatByMask(mask, e.currentTarget.value) : '');
    onChange ? onChange(e) : void 0;
  };
  return (
    <div className={'position-relative'}>
      <label>
        <span
          style={{
            position: 'absolute',
            top: '50%',
            transform: 'translateY(-50%)',
            left: 0,
            ...contentStyle,
          }}
          {...contentProps}
        >
          {val}
        </span>
        <Field
          style={{ color: 'transparent', ...style }}
          type={type}
          max={mask?.split('').filter((n) => n === maskedChar).length}
          onChange={handleChange}
          placeholder={mask?.replaceAll(maskedChar, '_')}
          {...rest}
        />
      </label>
    </div>
  );
};

MaskField.defaultProps = {
  mask: '+7-999-999-9999',
};
