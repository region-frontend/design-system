import { HTMLAttributes } from 'react';

import { FieldProps } from '../Field';

export type Props = FieldProps & {
  readonly mask?: string;
  readonly contentProps?: HTMLAttributes<HTMLSpanElement>;
};
