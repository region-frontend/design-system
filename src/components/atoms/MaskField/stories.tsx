import { Story } from '@storybook/react';
import React, { FormEvent, useState } from 'react';

import { MaskField } from './component';
import { MaskFieldProps } from './index';

export default {
  title: 'atoms/Form/MaskField',
  component: MaskField,
  argTypes: {
    mask: {
      description: 'Mask',
      control: 'text',
    },
    type: {
      description: 'Field type',
      options: ['text', 'number', 'textarea'],
      control: 'select',
    },
    disabled: {
      description: 'Disabled',
      control: 'boolean',
    },
  },
};

export const Appearance: Story<MaskFieldProps> = (args) => {
  const [value, setValue] = useState('');
  return (
    <>
      <MaskField
        onChange={(e: FormEvent<HTMLInputElement | HTMLTextAreaElement>) =>
          setValue(e.currentTarget.value)
        }
        style={{
          fontSize: 14,
        }}
        {...args}
      />

      <span>Value: {value}</span>
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {};
