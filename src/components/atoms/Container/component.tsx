import { css } from '@emotion/core';
import styled from '@emotion/styled';
import { FC } from 'react';

import { Props } from './props';

export const Container: FC<Props> = styled.div<Props>`
  ${({ size }) => css`
    max-width: ${size || 1000}px;
    margin: auto;
    position: relative;
    padding: 0 25px;
  `}
`;
