import React, { FC, useEffect, useState } from 'react';

import { getDays, getHours, getMinutes, getSeconds } from '../../../core/utils';
import { Props } from './props';

export const Timer: FC<Props> = ({
  pivotDate,
  onExpire,
  onWarning,
  stopOnExpire,
  warningBefore,
  format,
  separator = ':',
  ...rest
}: Props) => {
  const calculateDifference = (dateFromArg: number, dateToArg: number): number => {
    return dateFromArg - dateToArg;
  };
  const [expirationTriggered, setExpirationTriggered] = useState(false);

  const [dateDifference, setDateDifference] = useState<number>(
    calculateDifference(pivotDate.getTime(), new Date(Date.now()).getTime()),
  );

  // Set an interval for update date difference
  useEffect(() => {
    const interval = setInterval(() => {
      const tmpDifference = calculateDifference(
        pivotDate.getTime(),
        new Date(Date.now()).getTime(),
      );

      // Triggers when timer is expired
      if (tmpDifference < 0 && onExpire && !expirationTriggered) {
        onExpire();
        setExpirationTriggered(true);
      }

      // Triggers before n seconds
      if (getSeconds(tmpDifference) === warningBefore && onWarning) {
        onWarning();
      }

      // Stops the timer when expired, if it's needed
      if (stopOnExpire && tmpDifference < 0) {
        clearInterval(interval);
      } else {
        setDateDifference(tmpDifference);
      }
    }, 1000);
    return () => clearInterval(interval);
  }, [expirationTriggered, onExpire, onWarning, pivotDate, stopOnExpire, warningBefore]);

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  const renderBuilder = (dateDifferenceArg: number, result = '') => {
    const resultToArray = (resString: string): ReadonlyArray<number> =>
      resString.length ? resString.split(separator).map((n) => Number(n)) : [];
    return {
      withDays: () => {
        return renderBuilder(
          dateDifferenceArg,
          [getDays(dateDifferenceArg), ...resultToArray(result)].join(separator),
        );
      },
      withHours: () => {
        return renderBuilder(
          dateDifferenceArg,
          [
            ...result.split(separator).slice(0, 1),
            getHours(dateDifferenceArg) % 24,
            ...resultToArray(result).slice(2, resultToArray(result).length - 1),
          ].join(separator),
        );
      },
      withMinutes: () => {
        return renderBuilder(
          dateDifferenceArg,
          [
            ...resultToArray(result).slice(0, 2),
            getMinutes(dateDifferenceArg) % 60,
            ...resultToArray(result).slice(3, resultToArray(result).length - 1),
          ].join(separator),
        );
      },
      withSeconds: () => {
        return renderBuilder(
          dateDifferenceArg,
          [...resultToArray(result), getSeconds(dateDifferenceArg) % 60].join(separator),
        );
      },
      getResult: (): string => {
        return result;
      },
    };
  };

  const renderDirector = (dateDifferenceArg: number): string => {
    return Object.entries(
      format || {
        withDays: true,
        withHours: true,
        withMinutes: true,
        withSeconds: true,
      },
    )
      .reduce((acc, [key, value]) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return value ? acc[key]() : acc;
      }, renderBuilder(dateDifferenceArg))
      .getResult();
  };

  return <div {...rest}>{renderDirector(dateDifference)}</div>;
};
