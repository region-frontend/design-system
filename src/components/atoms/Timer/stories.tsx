import { Story } from '@storybook/react';
import React from 'react';
import { toast, ToastContainer } from 'react-toastify';

import { typography } from '../../../core';
import { Typography } from '../Typography';
import { Timer, TimerProps } from './index';

export default {
  title: 'atoms/Other/Timer',
  component: Timer,
};

export const Appearance: Story<TimerProps> = (args) => {
  const notify = (text: string) => (): void => {
    toast(text, {
      position: 'bottom-center',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: 'dark',
    });
  };

  return (
    <>
      <Typography as={'h1'} size={typography.Size.SmallHeading}>
        This will expire after 10 seconds
      </Typography>
      <Timer
        onExpire={notify('Timer has expired')}
        onWarning={notify('5 seconds left')}
        {...args}
      />
      <ToastContainer />
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  pivotDate: new Date(Date.now() + 10000),
  stopOnExpire: true,
  warningBefore: 5,
};
