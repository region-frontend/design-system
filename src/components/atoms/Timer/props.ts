import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly pivotDate: Date;
  readonly onExpire?: () => void;
  readonly onWarning?: () => void;
  readonly warningBefore?: number;
  readonly stopOnExpire?: boolean;
  readonly separator?: string;
  readonly format?: {
    readonly withDays?: boolean;
    readonly withHours?: boolean;
    readonly withMinutes?: boolean;
    readonly withSeconds?: boolean;
  };
};
