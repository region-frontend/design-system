import { Story } from '@storybook/react';
import React from 'react';

import { EmptyModuleSkeleton } from './component';
import { EmptyModuleSkeletonProps } from './index';

export default {
  title: 'atoms/Other/EmptyModuleSkeleton',
  component: EmptyModuleSkeleton,
};

export const Appearance: Story<EmptyModuleSkeletonProps> = () => {
  return (
    <>
      <EmptyModuleSkeleton label={'Список избранных объявлении пуст'} />
      <EmptyModuleSkeleton label={'Список избранных объявлении пуст'} title={'Спасибо!'} />
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {};
