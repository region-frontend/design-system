import React, { FC } from 'react';

import { colors, typography } from '../../../core';
import { Typography } from '../Typography';
import { Props } from './props';

export const EmptyModuleSkeleton: FC<Props> = ({ label, style, title, ...rest }: Props) => {
  return (
    <div
      style={{
        display: 'block',
        textAlign: 'center',
        ...style,
      }}
      {...rest}
    >
      <div
        style={{
          width: '50%',
          paddingBottom: '50%',
          borderRadius: '50%',
          backgroundColor: `${colors.Brand.Violet17}`,
          margin: '0 auto',
          marginBottom: 30,
        }}
      />
      {title ? (
        <Typography
          className={'mb-2'}
          as={'h2'}
          size={typography.Size.LargeHeading}
          style={{
            fontWeight: 600,
          }}
        >
          {title}
        </Typography>
      ) : null}
      <div
        style={{
          textAlign: 'center',
          fontSize: 16,
          fontWeight: 400,
          color: `${colors.Brand.Violet15}`,
        }}
      >
        {label}
      </div>
    </div>
  );
};
