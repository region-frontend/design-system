import { HTMLAttributes } from 'react';

export type Props = Omit<HTMLAttributes<HTMLDivElement>, 'title'> & {
  readonly label?: string;
  readonly title?: string;
};
