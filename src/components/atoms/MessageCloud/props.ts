import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly message?: string;
  readonly date?: Date | string;
  readonly isMe?: boolean;
};
