import { Story } from '@storybook/react';
import React from 'react';

import { MessageCloud } from './component';
import { MessageCloudProps } from './index';

export default {
  title: 'atoms/Feedback/MessageCloud',
  component: MessageCloud,
};

export const Appearance: Story<MessageCloudProps> = () => {
  return (
    <>
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <MessageCloud isMe={true} message={'Я хочу заказать полный салон'} date={'11:00'} />
        <MessageCloud
          className={'mt-3'}
          message={'Да, вы успеете. Пока что мне никто не написал.'}
          date={'11:00'}
        />
      </div>
    </>
  );
};
