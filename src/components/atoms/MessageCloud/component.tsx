import React, { FC } from 'react';

import { colors } from '../../../core';
import { Typography } from '../Typography';
import { MessageContainer } from './libs/MessageContainer';
import { Props } from './props';

export const MessageCloud: FC<Props> = ({ isMe, message, date, ...rest }: Props) => {
  return (
    <MessageContainer isMe={isMe} {...rest}>
      <Typography
        size={16}
        as={'span'}
        style={{
          color: `${isMe ? colors.Brand.White : colors.Text.MainBlack}`,
          fontWeight: 400,
        }}
      >
        {message}
      </Typography>
      <Typography
        size={11}
        as={'span'}
        style={{ color: colors.Text.GreySecondary, marginTop: 5, textAlign: 'right' }}
      >
        {date}
      </Typography>
    </MessageContainer>
  );
};
