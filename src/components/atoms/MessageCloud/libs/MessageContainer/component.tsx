import { css } from '@emotion/core';
import styled from '@emotion/styled';

import { colors } from '../../../../../core';
import { Props } from '../../props';

export const MessageContainer = styled.div<Props>`
  ${({ isMe }) =>
    css`
      background-color: ${isMe ? colors.Brand.MainColor : colors.Brand.Violet17};
      border-radius: 0.5rem;
      line-height: 1;
      width: fit-content;
      max-width: 75%;
      padding: 0.5rem 0.875rem;
      word-wrap: break-word;
      position: relative;
      display: flex;
      flex-direction: column;
      &:before {
        position: absolute;
        bottom: 0.2rem;
        content: '';
        height: 0.5rem;
      }
      &:after {
        position: absolute;
        bottom: 0.2rem;
        content: '';
        height: 0.5rem;
      }

      ${isMe
        ? css`
            align-self: flex-end;
            margin-right: 10px;
            &:before {
              border-bottom-left-radius: 0.8rem 0.7rem;
              border-right: 1rem solid ${colors.Brand.MainColor};
              right: -0.35rem;
              transform: translate(0, -0.1rem);
            }
            &:after {
              background-color: #fff;
              border-bottom-left-radius: 0.5rem;
              right: -40px;
              transform: translate(-30px, -2px);
              width: 10px;
            }
          `
        : css`
            align-self: flex-start;
            margin-left: 10px;
            &:before {
              border-bottom-right-radius: 0.8rem 0.7rem;
              border-left: 1rem solid ${colors.Brand.Violet17};
              left: -0.35rem;
              transform: translate(0, -0.1rem);
            }
            &:after {
              background-color: #fff;
              border-bottom-right-radius: 0.5rem;
              left: 20px;
              transform: translate(-30px, -2px);
              width: 10px;
            }
          `}
    `}
`;
