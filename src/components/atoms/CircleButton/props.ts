import { ButtonHTMLAttributes, ReactNode } from 'react';

export type Props = ButtonHTMLAttributes<HTMLButtonElement> & {
  readonly appearance?: 'default' | 'translucent' | 'primary';
  readonly icon?: ReactNode;
  readonly size?: number;
};
