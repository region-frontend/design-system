import React, { FC, useState } from 'react';

import { CircleButton } from './component';

export default {
  title: 'atoms/Buttons/CircleButton',
};

export const AppearanceTypes: FC = () => {
  const [count, setCount] = useState(0);

  const handleCounter = (): void => setCount(count + 1);

  return (
    <>
      <h2 className={'mt-2 mb-2'}>Default</h2>
      <CircleButton
        className="mr-2"
        onClick={handleCounter}
        appearance={'default'}
        icon={'👽'}
        size={50}
      />

      <CircleButton
        className="mr-2"
        onClick={handleCounter}
        appearance={'default'}
        icon={'👽'}
        size={75}
      />

      <CircleButton
        className="mr-2"
        onClick={handleCounter}
        appearance={'default'}
        icon={'👽'}
        size={100}
      />

      <h2 className={'mt-2 mb-2'}>Translucent</h2>
      <div
        className={'p-2'}
        style={{
          backgroundImage: `url("https://images.wallpaperscraft.ru/image/gradient_razmytost_abstraktsiia_157512_1920x1080.jpg")`,
          borderRadius: '10px',
        }}
      >
        <CircleButton
          className="mr-2"
          onClick={handleCounter}
          appearance={'translucent'}
          icon={'👽'}
          size={50}
        />

        <CircleButton
          className="mr-2"
          onClick={handleCounter}
          appearance={'translucent'}
          icon={'👽'}
          size={75}
        />

        <CircleButton
          className="mr-2"
          onClick={handleCounter}
          appearance={'translucent'}
          icon={'👽'}
          size={100}
        />
      </div>

      <h2 className={'mt-2 mb-2'}>Primary</h2>
      <CircleButton
        className="mr-2"
        onClick={handleCounter}
        appearance={'primary'}
        icon={'👽'}
        size={50}
      />

      <CircleButton
        className="mr-2"
        onClick={handleCounter}
        appearance={'primary'}
        icon={'👽'}
        size={75}
      />

      <CircleButton
        className="mr-2"
        onClick={handleCounter}
        appearance={'primary'}
        icon={'👽'}
        size={100}
      />
    </>
  );
};
