import { css } from '@emotion/core';
import styled from '@emotion/styled';
import React, { FC } from 'react';

import { colors } from '../../../core';
import { Props } from './props';

const CircleButtonBase: FC<Props> = ({ icon, children, ...rest }: Props) => (
  <button {...rest}>{icon || children}</button>
);

export const CircleButton = styled(CircleButtonBase)<Props>`
  ${({ appearance, size }) =>
    css`
      width: ${size || 50}px;
      height: ${size || 50}px;
      border: none;
      cursor: pointer;
      border-radius: ${size || 25}px;
      outline: none;
      box-shadow: 0 0 15px rgba(75, 70, 150, 0.1);
      ${
        appearance === 'primary' &&
        css`
          background: ${colors.Brand.MainColor};
          color: ${colors.Text.Light};
          &:hover {
            background: ${colors.Brand.MainColorDark};
          }
        `
      }
      ${
        appearance === 'default' &&
        css`
          background: ${colors.Background.White};
          color: ${colors.Brand.MainColor};
          &:hover {
            border-color: ${colors.Brand.MainColorDark};
            color: ${colors.Brand.MainColorDark};
          }
        `
      }
      ${
        appearance === 'translucent' &&
        css`
          background: rgba(255, 255, 255, 0.3);
          backdrop-filter: blur(8px);
          &:hover {
            border-color: ${colors.Brand.MainColorDark};
            color: ${colors.Brand.MainColorDark};
          }
        `
      }
    `}
`;

CircleButton.defaultProps = {
  appearance: 'primary',
};
