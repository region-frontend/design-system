import { Props } from './props';

export * from './component';

export type LoadingProps = Props;
