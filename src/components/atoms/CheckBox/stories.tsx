import { Story } from '@storybook/react';
import React, { ChangeEvent, useState } from 'react';

import { CheckBox } from './component';
import { CheckBoxProps } from './index';

export default {
  title: 'atoms/Form/CheckBox',
  component: CheckBox,
  argTypes: {
    active: {
      description: 'Is checkbox active',
      control: 'boolean',
    },
    label: {
      description: 'Label',
      control: 'text',
    },
    disabled: {
      description: 'Disabled',
      control: 'boolean',
    },
    labelPosition: {
      description: 'Label position',
      options: ['top', 'bottom', 'left', 'right'],
      control: 'select',
    },
  },
};

export const Appearance: Story<CheckBoxProps> = (args) => {
  const [value, setValue] = useState(true);
  return (
    <>
      <CheckBox
        onChange={(e: ChangeEvent<HTMLInputElement>) => setValue(e.currentTarget.checked)}
        active={value}
        wrapperProps={{
          style: {
            fontSize: 16,
          },
        }}
        {...args}
      />

      <span>Value: {value ? 'Checked' : 'Not checked'}</span>
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  label: 'CheckBox label',
};
