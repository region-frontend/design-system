/* @jsx jsx */
import { jsx } from '@emotion/core';
import classNames from 'classnames';
import { ChangeEvent, FC, useState } from 'react';

import { colors } from '../../../core';
import { Props } from './props';

export const CheckBox: FC<Props> = ({
  active,
  label,
  onChange,
  labelPosition,
  wrapperProps,
  style,
  disabled,
  ...rest
}: Props) => {
  const [isActive, setIsActive] = useState(active || false);
  const handleChange = (e: ChangeEvent<HTMLInputElement>): void => {
    setIsActive(e.currentTarget.checked);
    onChange ? onChange(e) : void 0;
  };
  return (
    <label
      className={classNames('d-flex', {
        'flex-column': labelPosition === 'top',
        'flex-column-reverse': labelPosition === 'bottom',
        'flex-row': labelPosition === 'left',
        'flex-row-reverse': labelPosition === 'right',
        'justify-content-end': labelPosition === 'right',
        'align-items-center': labelPosition && ['left', 'right'].includes(labelPosition),
      })}
      css={{
        gap: 10,
        color: colors.Text.Dark,
        fontSize: 13,
        cursor: 'pointer',
        '&:hover': {
          div:
            isActive && !disabled
              ? {
                  background: colors.Brand.MainColorDark,
                }
              : {
                  background: colors.Text.GreySecondary,
                },
        },
        ...wrapperProps?.style,
      }}
      {...wrapperProps}
    >
      {label && <span>{label}</span>}
      <input
        className={classNames('m-0', 'd-none')}
        type={'checkbox'}
        checked={isActive}
        onChange={handleChange}
        disabled={disabled}
        {...rest}
      />
      <div
        css={{
          width: 18,
          height: 18,
          borderRadius: 4,
          color: colors.Text.Light,
          textAlign: 'center',
          lineHeight: '15px',
          border: `2px solid ${colors.Text.GreySecondary}`,
          transition: 'background 0.1s, border-color 0.1s',
          ...(isActive
            ? {
                background: colors.Brand.MainColor,
                borderColor: colors.Brand.MainColor,
              }
            : {}),
          ...(disabled
            ? {
                background: colors.Text.GreySecondary,
              }
            : {}),
          ...style,
        }}
      >
        <span style={{ display: 'block', transform: 'translateX(1px) translateY(-1px)' }}>
          {isActive ? '✔' : ''}
        </span>
      </div>
    </label>
  );
};

CheckBox.defaultProps = {
  labelPosition: 'right',
};
