import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLInputElement> & {
  readonly active?: boolean;
  readonly disabled?: boolean;
  readonly label?: string;
  readonly labelPosition?: 'top' | 'bottom' | 'left' | 'right';
  readonly wrapperProps?: HTMLAttributes<HTMLLabelElement>;
};
