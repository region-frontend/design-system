import { Global } from '@emotion/core';
import { Drawer, SwipeableDrawer } from '@material-ui/core';
import React, { FC, useRef, useState } from 'react';
import { useSwipeable } from 'react-swipeable';

import { colors } from '../../../core';
import { CarrierCard } from '../CarrierCard';
import { Props } from './props';

export const SwipeModal: FC<Props> = ({
  children,
  drawerBleeding = 56,
  cardProps,
  borderRadius = 10,
  switchable,
  open: openProp,
  onOpen,
  onClose,
  ...rest
}: Props) => {
  const [open, setOpen] = useState(false);
  const [swipe, setSwipe] = useState(0);

  const toggleDrawer = (newOpen: boolean) => () => {
    setOpen(newOpen);
  };

  const bleeding = switchable ? 0 : drawerBleeding;

  const refBody = useRef<HTMLDivElement>(null);

  const DrawerComponent = switchable ? Drawer : SwipeableDrawer;

  const swipeHandlers = useSwipeable({
    onSwiping: (eventData) => {
      if (eventData.deltaY > 0 && refBody?.current?.scrollTop === 0) {
        setSwipe(eventData.deltaY);
      }
    },
    onSwipedDown: () => {
      if (swipe > 50) {
        onClose ? onClose() : toggleDrawer(false);
        setSwipe(0);
      } else {
        setSwipe(0);
      }
    },
  });

  return (
    <>
      <Global
        styles={{
          '.MuiPaper-root.MuiPaper-root': {
            height: `calc(100% - ${bleeding + 50}px)`,
            overflow: 'visible',
            borderRadius: `${borderRadius}px ${borderRadius}px 0 0`,
            ...(swipe && openProp ? { transform: `translateY(${swipe}px)!important` } : {}),
          },
        }}
      />
      <DrawerComponent
        container={window !== undefined ? () => window.document.body : undefined}
        anchor="bottom"
        open={openProp ?? open}
        onClose={onClose ?? toggleDrawer(false)}
        onOpen={onOpen ?? toggleDrawer(true)}
        swipeAreaWidth={bleeding}
        disableSwipeToOpen={false}
        ModalProps={{
          keepMounted: true,
        }}
        {...swipeHandlers}
        {...rest}
      >
        <div
          ref={refBody}
          style={{
            position: 'absolute',
            top: -bleeding,
            visibility: 'visible',
            right: 0,
            left: 0,
            zIndex: 999999,
            overflowY: 'auto',
            height: `calc(100% + ${bleeding}px)`,
          }}
        >
          <CarrierCard background={colors.Brand.White} borderRadius={10} {...cardProps}>
            {children}
          </CarrierCard>
        </div>
      </DrawerComponent>
    </>
  );
};
