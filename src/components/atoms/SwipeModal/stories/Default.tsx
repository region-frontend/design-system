import { Story } from '@storybook/react';
import React from 'react';

import { SwipeModal } from '../component';
import { SwipeModalProps } from '../index';

export default {
  title: 'atoms/SwipeModal/Default',
  component: SwipeModal,
};

export const Default: Story<SwipeModalProps> = (args) => {
  return (
    <>
      <h1>SwipeModal is on the bottom of the page</h1>
      <SwipeModal {...args}>
        <div className={'p-4'}>
          <h1>SwipeModal title</h1>
          <p>Lorem ipsum dolor sit amet</p>
          <p>Lorem ipsum dolor sit amet</p>
          <p>Lorem ipsum dolor sit amet</p>
          <p>Lorem ipsum dolor sit amet</p>
          <p>Lorem ipsum dolor sit amet</p>
          <p>Lorem ipsum dolor sit amet</p>
          <p>Lorem ipsum dolor sit amet</p>
          <p>Lorem ipsum dolor sit amet</p>
        </div>
      </SwipeModal>
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Default.args = {
  drawerBleeding: 100,
};
