import { Story } from '@storybook/react';
import { Button } from 'components';
import React, { useState } from 'react';

import { SwipeModal, SwipeModalProps } from '../index';

export default {
  title: 'atoms/SwipeModal/Switchable',
  component: SwipeModal,
};

export const Switchable: Story<SwipeModalProps> = (args) => {
  const [open, setOpen] = useState(false);
  return (
    <>
      <h1>SwipeModal is on the bottom of the page</h1>
      <Button onClick={() => setOpen((prev) => !prev)}>Open/Close</Button>
      <SwipeModal {...args} open={open} onClose={() => setOpen(false)} onOpen={() => setOpen(true)}>
        <div className={'p-4'}>
          <h1>SwipeModal title</h1>
          <p>Lorem ipsum dolor sit amet</p>
          <p>Lorem ipsum dolor sit amet</p>
          <p>Lorem ipsum dolor sit amet</p>
          <p>Lorem ipsum dolor sit amet</p>
          <p>Lorem ipsum dolor sit amet</p>
          <p>Lorem ipsum dolor sit amet</p>
          <p>Lorem ipsum dolor sit amet</p>
          <p>Lorem ipsum dolor sit amet</p>
        </div>
      </SwipeModal>
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Switchable.args = {
  switchable: true,
};
