import { SwipeableDrawerProps } from '@material-ui/core';
import { Optional } from 'utility-types';

import { CarrierCardProps } from '../CarrierCard';

export type Props = Omit<SwipeableDrawerProps, 'onClose' | 'onOpen' | 'open'> &
  Optional<Pick<SwipeableDrawerProps, 'onOpen' | 'open'>> & {
    readonly drawerBleeding?: number;
    readonly cardProps?: CarrierCardProps;
    readonly borderRadius?: number;
    readonly switchable?: boolean;
    readonly onClose?: () => void;
  };
