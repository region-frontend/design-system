import { CardProps } from '../Card';

export type Props = CardProps & {
  readonly src?: string;
};
