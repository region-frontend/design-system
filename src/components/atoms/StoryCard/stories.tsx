import { Story } from '@storybook/react';
import React from 'react';

import { StorybookAppearanceTypes } from '../../../core/storybook';
import { StoryCard } from './component';
import { StoryCardProps } from './index';

export default {
  title: 'atoms/Cards/StoryCard',
  component: StoryCard,
  argTypes: {
    borderSize: {
      description: 'Size of border (px)',
      table: {
        category: StorybookAppearanceTypes.Border,
        defaultValue: { summary: 1 },
      },
      control: 'number',
    },
    borderRadius: {
      description: 'Radius of border (px)',
      table: {
        category: StorybookAppearanceTypes.Border,
        defaultValue: { summary: 4 },
      },
      control: 'number',
    },
    disableShadow: {
      description: 'Whether component should have shadow or not',
      table: {
        defaultValue: { summary: false },
      },
      control: 'boolean',
    },
  },
};

export const Appearance: Story<StoryCardProps> = (args) => {
  return (
    <StoryCard
      className={'py-3 px-4'}
      src={
        'https://static.vecteezy.com/system/resources/previews/000/622/344/original/beautiful-background-of-lines-with-gradients-vector.jpg'
      }
      {...args}
    >
      <p style={{ color: 'white' }}>Lorem ipsum dolor sit amet</p>
    </StoryCard>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  disableShadow: false,
  borderRadius: 4,
  borderSize: 0,
};
