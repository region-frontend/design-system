import styled from '@emotion/styled';

import { Card } from '../Card';
import { Props } from './props';

export const StoryCard = styled(Card)<Props>`
  ${({ src }) => `
    background-image: url(${src});
    background-size: cover;
    background-position: center;
  `}
`;
