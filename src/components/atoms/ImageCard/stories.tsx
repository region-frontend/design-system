import { Story } from '@storybook/react';
import React from 'react';

import { Grid } from '../Grid';
import { ImageCard } from './component';
import { ImageCardProps } from './index';

export default {
  title: 'atoms/Cards/ImageCard',
  component: ImageCard,
  argTypes: {
    src: {
      control: 'text',
    },
  },
};

export const Appearance: Story<ImageCardProps> = () => {
  return (
    <>
      <h2 className={'mt-3 mb-3'}>Default</h2>
      <Grid columns={3}>
        <ImageCard
          borderRadius={10}
          src={'https://upload.wikimedia.org/wikipedia/commons/4/4e/Pleiades_large.jpg'}
        />
        <ImageCard
          borderRadius={10}
          src={'https://strg1.nm.kz/neofiles/serve-image/600a759c9d88d8000666e4c2/1190x500/q90'}
        />
        <ImageCard
          borderRadius={10}
          src={
            'https://s.auto.drom.ru/i24238/c/photos/fullsize/lexus/rx450h/lexus_rx450h_919805.jpg'
          }
        />
      </Grid>
    </>
  );
};
