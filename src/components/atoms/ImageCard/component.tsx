import React, { FC, useState } from 'react';

import { colors } from '../../../core';
import { Card, CircleButton } from '../../atoms';
import { Spinner } from '../Spinner';
import { Props } from './props';

export const ImageCard: FC<Props> = ({ src, alt, style, borderRadius = 15, ...rest }: Props) => {
  const [loading, setLoading] = useState(true);
  return (
    <>
      <Card
        style={{
          position: 'relative',
          overflow: 'hidden',
          display: 'flex',
          alignItems: 'center',
          ...(loading
            ? {
                background: colors.Brand.Violet17,
                padding: '50px 0',
              }
            : {
                backgroundImage: `url(${src})`,
                backgroundSize: 'cover',
                WebkitBackdropFilter: 'blur(10px)',
              }),
          ...style,
        }}
        borderRadius={borderRadius}
        {...rest}
      >
        {loading ? (
          <CircleButton
            appearance={'translucent'}
            style={{
              position: 'absolute',
              left: 'calc(50% - 13px)',
              top: 'calc(50% - 13px)',
              zIndex: 999999,
              cursor: 'default',
            }}
            onClick={() => void 0}
          >
            <Spinner />
          </CircleButton>
        ) : null}
        <div
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            backdropFilter: 'blur(8px) brightness(0.6)',
            borderRadius: borderRadius,
          }}
        />
        <img
          style={{
            ...(loading
              ? {
                  opacity: 0,
                }
              : {}),
            display: 'block',
            width: '100%',
            transition: 'opacity 0.3s',
            position: 'relative',
            zIndex: 10,
          }}
          loading={'eager'}
          onLoad={() => setLoading(false)}
          src={src}
          alt={alt || 'image'}
        />
      </Card>
    </>
  );
};
