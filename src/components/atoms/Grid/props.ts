import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly gutter?: number;
  readonly columns?: number;
};
