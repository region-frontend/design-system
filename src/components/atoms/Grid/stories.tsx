import { Story } from '@storybook/react';
import React from 'react';

import { colors } from '../../../core';
import { Card } from '../Card';
import { Grid } from './component';
import { GridProps } from './index';

export default {
  title: 'atoms/Layout/Grid',
  component: Grid,
  argTypes: {
    gutter: {
      description: 'Gutter',
      control: 'number',
    },
    columns: {
      description: 'Columns',
      control: 'number',
    },
  },
};

export const Appearance: Story<GridProps> = (args) => {
  return (
    <>
      <Grid {...args}>
        <Card style={{ height: 300, background: colors.Brand.MainColor }} borderRadius={10} />
        <Card style={{ height: 300, background: colors.Brand.MainColor }} borderRadius={10} />
        <Card style={{ height: 300, background: colors.Brand.MainColor }} borderRadius={10} />
        <Card style={{ height: 300, background: colors.Brand.MainColor }} borderRadius={10} />
        <Card style={{ height: 300, background: colors.Brand.MainColor }} borderRadius={10} />
        <Card style={{ height: 300, background: colors.Brand.MainColor }} borderRadius={10} />
        <Card style={{ height: 300, background: colors.Brand.MainColor }} borderRadius={10} />
        <Card style={{ height: 300, background: colors.Brand.MainColor }} borderRadius={10} />
      </Grid>
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {};
