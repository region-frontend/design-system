import React, { FC } from 'react';

import { emptyArrayGenerator } from '../../../core/utils';
import { Props } from './props';

export const Grid: FC<Props> = ({ gutter, columns, style, ...rest }: Props) => {
  return (
    <div
      style={{
        display: 'grid',
        gridTemplateColumns: emptyArrayGenerator(columns || 2).reduce((acc) => `${acc} 1fr`, ''),
        gap: gutter || 10,
        ...style,
      }}
      {...rest}
    />
  );
};
