import { Story } from '@storybook/react';
import React, { useState } from 'react';

import { PinCode } from './component';
import { PinCodeProps } from './index';

export default {
  title: 'atoms/Form/PinCode',
  component: PinCode,
  argTypes: {
    disabled: {
      description: 'Disabled',
      control: 'boolean',
    },
    isPassword: {
      description: 'Is slots have to be hidden',
      control: 'boolean',
    },
    length: {
      description: 'Max length',
      control: 'number',
    },
    gutter: {
      description: 'Gutter between slots',
      control: 'number',
    },
  },
};

export const Appearance: Story<PinCodeProps> = (args) => {
  const [val, setVal] = useState<number>();

  const onFilled = (pinCode: number): void => {
    alert(`Field filled, final value: ${pinCode}`);
  };

  return (
    <>
      <PinCode
        {...args}
        className={'mb-4'}
        onChange={(newVal) => setVal(newVal)}
        filledHandler={onFilled}
        size={30}
        slotPadding={20}
        alignCenter
        block
      />
      <span className={'mt-4'}>Value: {val}</span>
    </>
  );
};
