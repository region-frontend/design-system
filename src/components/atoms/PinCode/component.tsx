import styled from '@emotion/styled';
import React, { FC, FormEvent, useState } from 'react';

import { colors } from '../../../core';
import { emptyArrayGenerator } from '../../../core/utils';
import { Props } from './props';

const PinCodeBase: FC<Props> = ({
  onChange,
  length = 4,
  disabled,
  style,
  filledHandler,
  isPassword,
  gutter,
  alignCenter,
  size,
  block,
  slotPadding,
  ...rest
}: Props) => {
  const [val, setVal] = useState('');
  const [focused, setFocused] = useState(false);

  const handleChange = (e: FormEvent<HTMLInputElement>): void => {
    const eventValue = e.currentTarget.value.replace(/[^0-9]/g, '');
    setVal(eventValue);
    onChange ? onChange(eventValue ? Number(eventValue) : undefined) : void 0;
    eventValue.length === length && filledHandler ? filledHandler(Number(eventValue)) : void 0;
  };

  return (
    <label
      style={{
        position: 'relative',
        cursor: 'pointer',
        ...(alignCenter
          ? {
              justifyContent: 'center',
            }
          : {}),
        ...(style || {}),
      }}
      {...rest}
    >
      <div
        className={block ? 'd-flex w-100 justify-content-between' : 'd-inline-flex'}
        style={{
          gap: gutter || 5,
        }}
      >
        {emptyArrayGenerator(length).map((n, i) => (
          <div
            key={i}
            style={{
              borderBottom: `2px solid ${
                focused ? colors.Brand.MainColor : colors.Text.GreySecondary
              }`,
              fontSize: size || 20,
              textAlign: 'center',
              padding: slotPadding || '5px 10px',
              color: val.split('')[n] ? colors.Text.Dark : colors.Background.Transparent,
              transition: 'border-color 0.2s',
              ...(block
                ? {
                    flex: 1,
                  }
                : {}),
            }}
          >
            {(isPassword ? '•' : val.split('')[n]) || '0'}
          </div>
        ))}
      </div>
      <input
        type={'text'}
        onChange={handleChange}
        maxLength={length}
        disabled={disabled}
        inputMode={'numeric'}
        style={{
          width: 0,
          height: 0,
          border: 'none',
          position: 'absolute',
          opacity: 0,
        }}
        value={val}
        onFocus={() => setFocused(true)}
        onBlur={() => setFocused(false)}
      />
    </label>
  );
};

export const PinCode = styled(PinCodeBase)`
  display: flex;
`;

PinCode.defaultProps = {
  length: 4,
};
