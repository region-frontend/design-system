import { HTMLAttributes } from 'react';

export type Props = Omit<HTMLAttributes<HTMLLabelElement>, 'onChange' | 'block' | 'size'> & {
  readonly length?: number;
  readonly disabled?: boolean;
  readonly onChange?: (newValue: number | undefined) => void;
  readonly isPassword?: boolean;
  readonly filledHandler?: (value: number) => void;
  readonly gutter?: number;
  readonly alignCenter?: boolean;
  readonly block?: boolean;
  readonly size?: number;
  readonly slotPadding?: number;
};
