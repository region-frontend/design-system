import { Story } from '@storybook/react';
import React from 'react';

import { Avatar } from './component';
import { AvatarProps } from './index';

export default {
  title: 'atoms/Avatar',
  component: Avatar,
};

export const Appearance: Story<AvatarProps> = (args) => {
  return <Avatar {...args} />;
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  url:
    'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png',
};
