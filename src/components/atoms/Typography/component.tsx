import { css } from '@emotion/core';
import styled from '@emotion/styled';
import { createElement, FC } from 'react';

import { colors } from '../../../core';
import { Props } from './props';

const TypographyBase: FC<Props> = ({ as, children, ...rest }: Props) =>
  createElement(as, rest, children);

export const Typography = styled(TypographyBase)<Props>`
  ${({ size }) => css`
    color: ${colors.Text.MainBlack};
    font-size: ${size}px;
  `}
`;
