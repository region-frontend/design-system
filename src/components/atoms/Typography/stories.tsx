import { Story } from '@storybook/react';
import React from 'react';

import { typography } from '../../../core';
import { Typography } from './component';
import { TypographyProps } from './index';

export default {
  title: 'atoms/Other/Typography',
  component: Typography,
  argTypes: {
    as: {
      options: ['h1', 'h2', 'h3', 'h5', 'h6', 'p', 'span'],
      control: 'select',
    },
  },
};

export const Appearance: Story<TypographyProps> = (args) => {
  return (
    <Typography className={'py-3 px-4'} {...args}>
      Lorem ipsum dolor sit amet
    </Typography>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  as: 'h1',
  size: typography.Size.HugeText,
};
