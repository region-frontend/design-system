import { HTMLAttributes } from 'react';

import { typography } from '../../../core';

export type Props = HTMLAttributes<HTMLElement> & {
  readonly as: keyof JSX.IntrinsicElements;
  readonly size: typography.Size;
};
