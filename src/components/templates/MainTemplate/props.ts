import { HTMLAttributes } from 'react';

import { ModuleNavigationProps } from '../../organisms';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly navigationProps: ModuleNavigationProps;
};
