import React, { FC } from 'react';

import { ModuleNavigation } from '../../organisms';
import { Props } from './props';

export const MainTemplate: FC<Props> = ({ navigationProps, children, style, ...rest }: Props) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        ...style,
      }}
      {...rest}
    >
      <div
        style={{
          flex: 1,
          overflowY: 'auto',
        }}
      >
        {children}
      </div>
      <ModuleNavigation {...navigationProps} />
    </div>
  );
};
