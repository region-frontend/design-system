import {
  faBars,
  faBox,
  faComment,
  faHeart,
  faHome,
  faPlus,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Story } from '@storybook/react';
import React, { useState } from 'react';

import { CircleButton } from '../../atoms';
import { MainTemplate } from '../MainTemplate';
import { RootTemplate, RootTemplateProps } from './index';

export default {
  title: 'templates/RootTemplate',
  component: RootTemplate,
};

export const Appearance: Story<RootTemplateProps> = (args) => {
  const [isActive, setIsActive] = useState(false);
  return (
    <>
      <RootTemplate {...args} active={isActive} onClose={() => setIsActive(false)}>
        <MainTemplate
          navigationProps={{
            centerItem: {
              icon: <FontAwesomeIcon icon={faPlus} />,
              onClick: () => alert('Plus clicked'),
            },
            leftItems: [
              {
                icon: <FontAwesomeIcon icon={faHome} />,
                label: 'Главная',
                isActive: true,
                onClick: () => alert('Главная clicked'),
              },
              {
                icon: <FontAwesomeIcon icon={faHeart} />,
                label: 'Избранные',
                onClick: () => alert('Избранные clicked'),
              },
            ],
            rightItems: [
              {
                icon: <FontAwesomeIcon icon={faBox} />,
                label: 'Мои объявления',
                onClick: () => alert('Мои объявления clicked'),
              },
              {
                icon: <FontAwesomeIcon icon={faComment} />,
                label: 'Сообщения',
                onClick: () => alert('Сообщения clicked'),
              },
            ],
          }}
        >
          <CircleButton appearance={'default'} onClick={() => setIsActive(true)}>
            <FontAwesomeIcon icon={faBars} />
          </CircleButton>

          <span>Content</span>
          <p style={{ fontSize: 40 }}>
            Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
            Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
            Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
            Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
            Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
            Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
            Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
            Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
            Lorem ipsum Lorem ipsum Lorem ipsum
          </p>
        </MainTemplate>
      </RootTemplate>
    </>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  navigationProps: [
    {
      label: 'Главная',
      isActive: true,
      onClick: () => alert('Главная clicked'),
    },
    {
      label: 'Избранные',
      onClick: () => alert('Избранные clicked'),
    },
  ],
  footerProps: [
    {
      label: 'Выйти',
      isActive: true,
      onClick: () => alert('Выйти clicked'),
    },
  ],
  phoneNumber: '+7 708 127 4423',
};
