import React, { FC } from 'react';

import { colors, typography } from '../../../core';
import { Card, Divider } from '../../atoms';
import { Props } from './props';

export const RootTemplate: FC<Props> = ({
  navigationProps,
  footerProps,
  phoneNumber,
  children,
  style,
  active,
  onClose,
  ...rest
}: Props) => {
  const radius = 20;

  return (
    <div
      style={{
        height: '100vh',
        overflow: 'hidden',
        ...style,
      }}
      {...rest}
    >
      <div style={{ height: '100%', whiteSpace: 'nowrap', lineHeight: 0 }}>
        <div
          style={{
            display: 'inline-block',
            position: 'relative',
            background: colors.Background.White,
            height: '100%',
            lineHeight: 'initial',
            width: 300,
            left: !active ? -300 : 0,
            transition: 'left 0.3s',
            padding: 15,
            float: 'left',
          }}
        >
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              height: '100%',
            }}
          >
            <div />
            <div
              style={{
                color: colors.Brand.MainColor,
                fontSize: typography.Size.RegularHeading,
                padding: '20px 0',
              }}
            >
              {phoneNumber}
            </div>
            <Divider />
            <div style={{ flex: 1 }}>
              {navigationProps.map((n, i) => (
                <button
                  style={{
                    display: 'block',
                    background: 'transparent',
                    border: 0,
                    fontSize: typography.Size.LargeText,
                    color: n.isActive ? colors.Brand.MainColor : colors.Text.MainBlack,
                    padding: '10px 0',
                    cursor: 'pointer',
                  }}
                  onClick={n.onClick}
                  key={i}
                >
                  {n.label}
                </button>
              ))}
            </div>
            <Divider />
            <div>
              {footerProps.map((n, i) => (
                <button
                  style={{
                    display: 'block',
                    background: 'transparent',
                    border: 0,
                    fontSize: typography.Size.LargeText,
                    color: n.isActive ? colors.Brand.MainColor : colors.Text.MainBlack,
                    padding: '10px 0',
                    cursor: 'pointer',
                  }}
                  onClick={n.onClick}
                  key={i}
                >
                  {n.label}
                </button>
              ))}
            </div>
          </div>
        </div>
        <div
          style={{
            display: 'inline-block',
            width: '100%',
            height: '100%',
            lineHeight: 'initial',
            transition: 'left 0.3s',
            position: 'relative',
            left: !active ? -300 : 0,
          }}
        >
          <Card
            style={{
              height: '100%',
              position: 'relative',
              transition: 'transform 0.3s',
              willChange: 'transform',
              borderRadius: radius,
              ...(active
                ? {
                    transform: 'scale(.95)',
                  }
                : {}),
            }}
          >
            {active && (
              <div
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  width: '100%',
                  height: '100%',
                  zIndex: 9999,
                  borderRadius: radius,
                  background: colors.Background.Overlay,
                  transition: 'background 0.3s',
                }}
                onClick={onClose}
              />
            )}
            {React.cloneElement(children, {
              style: {
                borderRadius: active ? radius : 0,
                overflow: 'hidden',
                whiteSpace: 'initial',
              },
            })}
          </Card>
        </div>
      </div>
    </div>
  );
};
