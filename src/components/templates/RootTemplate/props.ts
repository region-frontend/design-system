import { HTMLAttributes, ReactElement } from 'react';

import { ModuleNavigationItemProps } from '../../organisms';

export type Props = Omit<HTMLAttributes<HTMLDivElement>, 'children' | 'active'> & {
  readonly navigationProps: ReadonlyArray<
    Pick<ModuleNavigationItemProps, 'isActive' | 'label' | 'onClick'>
  >;
  readonly footerProps: ReadonlyArray<
    Pick<ModuleNavigationItemProps, 'isActive' | 'label' | 'onClick'>
  >;
  readonly phoneNumber: string;
  readonly children: ReactElement;
  readonly active?: boolean;
  readonly onClose?: () => void;
};
