# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.13.2](https://gitlab.com/region-frontend/design-system/compare/v2.13.1...v2.13.2) (2021-11-24)


### Bug Fixes

* swipe modal multiple ([1989fca](https://gitlab.com/region-frontend/design-system/commits/1989fcac246c37c1c9a48067593bc2a441e4a1db))

### [2.13.1](https://gitlab.com/region-frontend/design-system/compare/v2.13.0...v2.13.1) (2021-11-02)


### Bug Fixes

* delete spacing in module navigation ([618cf80](https://gitlab.com/region-frontend/design-system/commits/618cf80a8925ecebf1545c015ec58dcb99a088b9))

## [2.13.0](https://gitlab.com/region-frontend/design-system/compare/v2.12.0...v2.13.0) (2021-11-02)


### Features

* add content props to mask field ([2108613](https://gitlab.com/region-frontend/design-system/commits/2108613032fe0f54386c01d9eca0641fcc430046))

## [2.12.0](https://gitlab.com/region-frontend/design-system/compare/v2.11.14...v2.12.0) (2021-10-25)


### Features

* add scroll for SwipeModal ([448988d](https://gitlab.com/region-frontend/design-system/commits/448988d17297e1882bcf4a0e240f3334328eea3b))
* update typescript ([8dd9878](https://gitlab.com/region-frontend/design-system/commits/8dd98784eaf0ff4d473a97d4bcaacaad3e28e1a3))

### [2.11.14](https://gitlab.com/region-frontend/design-system/compare/v2.11.13...v2.11.14) (2021-10-24)


### Bug Fixes

* add title to EmptyModuleSkeleton ([508dd12](https://gitlab.com/region-frontend/design-system/commits/508dd12216bcfcdd6f385c1519b1912f0d080a48))

### [2.11.13](https://gitlab.com/region-frontend/design-system/compare/v2.11.12...v2.11.13) (2021-10-24)


### Bug Fixes

* border radius of ImageCard ([dca98ff](https://gitlab.com/region-frontend/design-system/commits/dca98ff60afd111e63a9b34c634c519887c30520))

### [2.11.12](https://gitlab.com/region-frontend/design-system/compare/v2.11.11...v2.11.12) (2021-10-24)


### Bug Fixes

* excess SwipeModal shadow ([43af5b8](https://gitlab.com/region-frontend/design-system/commits/43af5b81753db38d0dab37b5ba28f9fbdcc6c4d7))

### [2.11.11](https://gitlab.com/region-frontend/design-system/compare/v2.11.10...v2.11.11) (2021-10-24)


### Bug Fixes

* add suffix icon for Field ([cac4b77](https://gitlab.com/region-frontend/design-system/commits/cac4b774acc6a82e3555815a399bffb4c9fa386a))

### [2.11.10](https://gitlab.com/region-frontend/design-system/compare/v2.11.9...v2.11.10) (2021-10-24)


### Bug Fixes

* field placeholder size ([9a85827](https://gitlab.com/region-frontend/design-system/commits/9a858279f7f2392d61f60c878db34e5672aca89e))

### [2.11.9](https://gitlab.com/region-frontend/design-system/compare/v2.11.8...v2.11.9) (2021-10-24)


### Bug Fixes

* field border size and border color when inactive ([95001ce](https://gitlab.com/region-frontend/design-system/commits/95001ce5ec4471df9f47623994d39b91ed3964b0))

### [2.11.8](https://gitlab.com/region-frontend/design-system/compare/v2.11.7...v2.11.8) (2021-10-23)


### Bug Fixes

* field value dependency on value prop ([20903c6](https://gitlab.com/region-frontend/design-system/commits/20903c63f3dfaf4c007267ee8a08c7253e0e80ab))

### [2.11.7](https://gitlab.com/region-frontend/design-system/compare/v2.11.6...v2.11.7) (2021-10-23)


### Bug Fixes

* field value dependency on value prop ([a34af04](https://gitlab.com/region-frontend/design-system/commits/a34af04510899cb2d3b8ae9008eefad3c9c2b327))

### [2.11.6](https://gitlab.com/region-frontend/design-system/compare/v2.11.5...v2.11.6) (2021-10-23)


### Bug Fixes

* userRating add comment ([9a07c43](https://gitlab.com/region-frontend/design-system/commits/9a07c439698fe255798647e414cd8685ecf8c0c5))

### [2.11.5](https://gitlab.com/region-frontend/design-system/compare/v2.11.4...v2.11.5) (2021-10-23)


### Bug Fixes

* userRating disabled prop ([31939e7](https://gitlab.com/region-frontend/design-system/commits/31939e750090427b38b086948420cc2e42bcc66f))

### [2.11.4](https://gitlab.com/region-frontend/design-system/compare/v2.11.3...v2.11.4) (2021-10-23)

### [2.11.3](https://gitlab.com/region-frontend/design-system/compare/v2.11.2...v2.11.3) (2021-10-23)


### Bug Fixes

* styles for Modal ([806be0e](https://gitlab.com/region-frontend/design-system/commits/806be0e7c4622b17ac261ca89d622746b67e0c3d))

### [2.11.2](https://gitlab.com/region-frontend/design-system/compare/v2.11.1...v2.11.2) (2021-10-08)


### Bug Fixes

* header circle button zIndex ([ffd4b78](https://gitlab.com/region-frontend/design-system/commits/ffd4b78879b745148a65949320cf4ce90bb74b3a))

### [2.11.1](https://gitlab.com/region-frontend/design-system/compare/v2.11.0...v2.11.1) (2021-10-08)


### Bug Fixes

* default value for Field and ImageCard background blur fix ([65e48f1](https://gitlab.com/region-frontend/design-system/commits/65e48f17ecbc37e8d4204abbd77984965f868858))

## [2.11.0](https://gitlab.com/region-frontend/design-system/compare/v2.10.0...v2.11.0) (2021-10-08)


### Features

* destructure styles for InPageTabulation ([bb60ea5](https://gitlab.com/region-frontend/design-system/commits/bb60ea5ef882260420548106b02c044fdea4530a))

## [2.10.0](https://gitlab.com/region-frontend/design-system/compare/v2.9.0...v2.10.0) (2021-10-07)


### Features

* add Violet5 color ([68b22b4](https://gitlab.com/region-frontend/design-system/commits/68b22b4f32ee9dec4a9b75b8b37af4891b0f98ac))

## [2.9.0](https://gitlab.com/region-frontend/design-system/compare/v2.8.2...v2.9.0) (2021-10-07)


### Features

* password type for Field, add DarkGrey and RedIndicator colors ([6a3747d](https://gitlab.com/region-frontend/design-system/commits/6a3747d5f715e815044dc269554efbe208646f48))

### [2.8.2](https://gitlab.com/region-frontend/design-system/compare/v2.8.1...v2.8.2) (2021-10-04)


### Bug Fixes

* add horizontal padding for Container ([d310ff6](https://gitlab.com/region-frontend/design-system/commits/d310ff6336737882e30da878a242b792098ec4c9))

### [2.8.1](https://gitlab.com/region-frontend/design-system/compare/v2.8.0...v2.8.1) (2021-10-04)


### Bug Fixes

* focus styles ([54e2442](https://gitlab.com/region-frontend/design-system/commits/54e2442fcd3237de94d64a28a6ab7b6c3f85b352))

## [2.8.0](https://gitlab.com/region-frontend/design-system/compare/v2.7.2...v2.8.0) (2021-10-04)


### Features

* add external control for RootTemplate ([4ba1a8d](https://gitlab.com/region-frontend/design-system/commits/4ba1a8d799903afd80ebe88e1207350fb87c5f06))

### [2.7.2](https://gitlab.com/region-frontend/design-system/compare/v2.7.1...v2.7.2) (2021-10-03)


### Bug Fixes

* message tail ledge of it's parent ([2d6055f](https://gitlab.com/region-frontend/design-system/commits/2d6055fd707a3d129667eaa72899f80f86d39e3f))
* style issues of AdListing ([78b7009](https://gitlab.com/region-frontend/design-system/commits/78b70091310ff7aeed3fc4f128b9a100e49179c1))
* style issues of SimpleField, focus styling ([45cbc10](https://gitlab.com/region-frontend/design-system/commits/45cbc105a7101c90c7b5701c14c8c82acca0c0a0))

### [2.7.1](https://gitlab.com/region-frontend/design-system/compare/v2.7.0...v2.7.1) (2021-10-03)


### Bug Fixes

* switchable props for SwipeModal ([ac5c2e7](https://gitlab.com/region-frontend/design-system/commits/ac5c2e76e5f10549e6ca554ec63a826083e0212a))

## [2.7.0](https://gitlab.com/region-frontend/design-system/compare/v2.6.2...v2.7.0) (2021-10-02)


### Features

* add AdListing component ([8689f16](https://gitlab.com/region-frontend/design-system/commits/8689f166452026066de5d1f4191f950a5ad7775d))


### Bug Fixes

* export AdListing to global scope ([21874ba](https://gitlab.com/region-frontend/design-system/commits/21874bad1a33003fb0dba98ba8688be89fdc3695))

### [2.6.2](https://gitlab.com/region-frontend/design-system/compare/v2.6.1...v2.6.2) (2021-09-29)


### Bug Fixes

* size, padding of PinCode ([1c33799](https://gitlab.com/region-frontend/design-system/commits/1c3379967e0cb666603da0cdfc80dca9d9b6f75f))

### [2.6.1](https://gitlab.com/region-frontend/design-system/compare/v2.6.0...v2.6.1) (2021-09-27)

## [2.6.0](https://gitlab.com/region-frontend/design-system/compare/v2.5.0...v2.6.0) (2021-09-27)


### Features

* active modifier for RootTemplate links ([b87f45b](https://gitlab.com/region-frontend/design-system/commits/b87f45bcefac7a9bcd73c37d738c576668a66d46))

## [2.5.0](https://gitlab.com/region-frontend/design-system/compare/v2.4.0...v2.5.0) (2021-09-26)


### Features

* container for MainNavigation ([867e36e](https://gitlab.com/region-frontend/design-system/commits/867e36e0d3a7774bb87bb5cdf4ed4622d5ad6039))


### Bug Fixes

* bad layout of RootTemplate ([4555d0d](https://gitlab.com/region-frontend/design-system/commits/4555d0d2e4de64bd57ff566b0d3f8cd4e8a22f2c))

## [2.4.0](https://gitlab.com/region-frontend/design-system/compare/v2.3.0...v2.4.0) (2021-09-26)


### Features

* add RootTemplate ([a5b43b0](https://gitlab.com/region-frontend/design-system/commits/a5b43b049c0ee1e844d1cec22847ff21a9e6aa3b))

## [2.3.0](https://gitlab.com/region-frontend/design-system/compare/v2.2.4...v2.3.0) (2021-09-24)


### Features

* add export for templates module ([f4252ee](https://gitlab.com/region-frontend/design-system/commits/f4252eeb12349de538aa3c56914b0aac9a749ed6))
* add releaser tool ([5bc6257](https://gitlab.com/region-frontend/design-system/commits/5bc6257ca0dda40ed412659e6681679a6b9b578e))
* add tag pushing for release ([ef682a6](https://gitlab.com/region-frontend/design-system/commits/ef682a6099b67c825894599a0d94f6df0738c6fd))
* add versionrc file ([f5b7871](https://gitlab.com/region-frontend/design-system/commits/f5b78710cc0bca2afd0d7abbb34989e1d8459e3e))
* move several components ([82614ec](https://gitlab.com/region-frontend/design-system/commits/82614ec7a0d459472c6b5e838e56cf463e47f34b))


### Bug Fixes

* absolute imports ([1941d9e](https://gitlab.com/region-frontend/design-system/commits/1941d9e5cfae725d483c9eb377136459aa909cd8))
* delete CHANGELOG.md ([9c4b987](https://gitlab.com/region-frontend/design-system/commits/9c4b9870042ebf7f785450e24bd09cb4fe07d72c))
* release script ([0a53628](https://gitlab.com/region-frontend/design-system/commits/0a53628b1db680aeeb7a9d2a6b798eca202eeeaf))
* versionrc url to design system url ([e5bdb40](https://gitlab.com/region-frontend/design-system/commits/e5bdb403b37a6c0b955b4d05d872510d26584aeb))
* versionrc urls ([5af7fdf](https://gitlab.com/region-frontend/design-system/commits/5af7fdf34dd08c1b4ab491ff947ec8efaadbfbfc))

### [2.2.4](https://github.com/mokkapps/changelog-generator-demo/compare/v2.2.3...v2.2.4) (2021-09-24)


### Bug Fixes

* release script ([03f16d4](https://github.com/mokkapps/changelog-generator-demo/commits/03f16d4d029d4ab85a8c8ad71afa665b546b07f4))

### [2.2.3](https://github.com/mokkapps/changelog-generator-demo/compare/v2.2.2...v2.2.3) (2021-09-24)

### [2.2.1](https://github.com/mokkapps/changelog-generator-demo/compare/v2.2.0...v2.2.1) (2021-09-24)


### Bug Fixes

* delete CHANGELOG.md ([b5eab02](https://github.com/mokkapps/changelog-generator-demo/commits/b5eab024fc4d9af97a65b7396d276b4a5e091c7f))
