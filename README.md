## Available Scripts

In the project directory, you can run:

### `yarn ci`

Checks all files for matching rules provided in config files of the project.
Runs both `lint` and `test` checks. You can run each of them manually, giving some clarification like `yarn ci:lint` or `yarn ci:test`

### `yarn compile`

Compiles all typescript files and puts them into `./lib` folder.

### `yarn clean`

Deletes `./lib` folder

### `yarn start`

**Note: This is development only command!**

Runs Storybook UI, providing development environment, for convenient development and testing of components, without importing them into production pages.
